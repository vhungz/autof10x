/** 
*   @file           Spi_Cfg.h
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/
#ifndef SPI_CFG_H
#define SPI_CFG_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Std_Types.h"
#include "Spi_Types.h"
/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Mcl driver header file and also in the
*        module's description file
*/
#define SPI_CFG_H_VENDOR_ID                             1
#define SPI_CFG_H_AR_RELEASE_MAJOR_VERSION              4
#define SPI_CFG_H_AR_RELEASE_MINOR_VERSION              3
#define SPI_CFG_H_AR_RELEASE_REVISION_VERSION           1
#define SPI_CFG_H_SW_MAJOR_VERSION                      0
#define SPI_CFG_H_SW_MINOR_VERSION                      9
#define SPI_CFG_H_SW_PATCH_VERSION                      0



/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/



/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/

#define SPI_PRECOMPILE_SUPPORT             (STD_ON)

/**
* @brief Enable/Disable Development Error Detection
*
* @implements   SPI_DEV_ERROR_DETECT_define
*/
#define SPI_DEV_ERROR_DETECT               (STD_OFF)


/**
* @brief Use/remove Spi_GetVersionInfo function from the compiled driver
*
* @implements   SPI_VERSION_INFO_API_define
*/
#define SPI_VERSION_INFO_API               (STD_ON)


/**
* @brief   An hardware error occurred during asynchronous or synchronous SPI transmit.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_HARDWARE_ERROR               ($SPI_E_HARDWARE_ERROR)

#define SPI_TOTAL_CHANNEL 1
#define SPI_TOTAL_JOB 1
#define SPI_TOTAL_SEQUENCE 1
#define SPI_TOTAL_DEVICE 1
#define SPI_TOTAL_PHY_UNIT 1
#define SPI_PHYUNIT_SPI1 0
#define SPI_CHANNEL_EB_BUFFER 0
#define SPI_DEVICE_NFC 0
#define SPI_JOB_NFC_JOB 0
#define SPI_SEQUENCE_NFC_SEQ 0


/*==================================================================================================
*                                              ENUMS
==================================================================================================*/


/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/
#if SPI_PRECOMPILE_SUPPORT == STD_ON
extern VAR(Spi_ConfigType, SPI_VAR) Spi_PBCfgVariantPredefined;
#endif /* SPI_PRECOMPILE_SUPPORT == STD_OFF */
extern VAR(Spi_ChannelConfigType, SPI_VAR) Spi_ChannelConfigs[];
extern VAR(Spi_SequenceConfigType, SPI_VAR) Spi_SequenceConfigs[];
extern VAR(Spi_PhyHwUnitType, SPI_VAR) Spi_PhyHwUnits[];
/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/


#ifdef __cplusplus
}
#endif

#endif /* #ifndef SPI_H */

/** @} */  

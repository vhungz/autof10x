/** 
*   @file           Spi_Cfg.c
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/

#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Spi_Cfg.h"
#include "Spi_REG.h"
#include "StdRegMacros.h"
#ifdef SPI_ENABLE_DMA
#if (SPI_ENABLE_DMA == STD_ON)
#include "Mcl_Cfg.h" 
#endif
#endif
#ifdef SPI_ENABLE_CS_GPIO
#if (SPI_ENABLE_CS_GPIO == STD_ON)
#include "Dio.h"
#endif
#endif
/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Mcl driver header file and also in the
*        module's description file
*/
#define SPI_CFG_C_VENDOR_ID                             1
#define SPI_CFG_C_AR_RELEASE_MAJOR_VERSION              4
#define SPI_CFG_C_AR_RELEASE_MINOR_VERSION              3
#define SPI_CFG_C_AR_RELEASE_REVISION_VERSION           1
#define SPI_CFG_C_SW_MAJOR_VERSION                      0
#define SPI_CFG_C_SW_MINOR_VERSION                      9
#define SPI_CFG_C_SW_PATCH_VERSION                      0



/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
/* Check if Spi_Cfg.c and Spi_Cfg.h file are of the same Autosar version */
#if ((SPI_CFG_C_AR_RELEASE_MAJOR_VERSION    != SPI_CFG_H_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_CFG_C_AR_RELEASE_MINOR_VERSION    != SPI_CFG_H_AR_RELEASE_MINOR_VERSION) || \
     (SPI_CFG_C_AR_RELEASE_REVISION_VERSION != SPI_CFG_H_AR_RELEASE_REVISION_VERSION) \
    )
    #error "AutoSar Version Numbers of Spi_Cfg.c and Spi_Cfg.h are different"
#endif
/* Check if Spi_Cfg.c and Spi_Cfg.h header file are of the same Software version */
#if ((SPI_CFG_C_SW_MAJOR_VERSION != SPI_CFG_H_SW_MAJOR_VERSION) || \
     (SPI_CFG_C_SW_MINOR_VERSION != SPI_CFG_H_SW_MINOR_VERSION) || \
     (SPI_CFG_C_SW_PATCH_VERSION != SPI_CFG_H_SW_PATCH_VERSION)    \
    )
    #error "Software Version Numbers of Spi_Cfg.c and Spi_Cfg.h are different"
#endif




#if (STD_ON == SPI_PRECOMPILE_SUPPORT)
/*==================================================================================================
*                         LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/

static VAR(Spi_BufferDescriptorType, SPI_VAR) EBBuffer_Channel_EB_BUFFER;









/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/
#define SPI_START_SEC_VAR_INIT_UNSPECIFIED
//#include "Spi_MemMap.h"

VAR(Spi_PhyHwUnitType, SPI_VAR) Spi_PhyHwUnits[] = {
    /* SPI_PHYUNIT_SPI1*/
    {
        (0x40013000), /* Hw Unit */
        (1), /* Config */
        SPI_UNINIT, /* Status */
        NULL_PTR, /* Current job */
        0, /* DMA TX Channel*/
        0 /* DMA RX Channel*/
    },
};



VAR(Spi_ChannelConfigType, SPI_VAR) Spi_ChannelConfigs[] = {
    /* SPI_CHANNEL_EB_BUFFER*/
    {
        (1 | (0 << 1)), /* Config */
        (8), /* Data Width */
        (0xFF), /* Default Data */
        0, /* Current Index */
        0, /* Recv Index */
        0, /* Buffer Length*/
        (1000), /* Max Buffer Length*/
        (&EBBuffer_Channel_EB_BUFFER), /* Buffer*/
    },

};



VAR(Spi_ExternalDeviceConfigType, SPI_VAR) Spi_DeviceConfigs[] = {
    /* SPI_DEVICE_NFC*/
    {
        (
            (1) /* CS Polarity */
             | (1 << 1) /* CS Selection */
             | (1 << 2) /* Enable CS */
             | (1 << 3) /* Data Shift Edge */
             | (1 << 4) /* Shift Clock Idle Level */
        ), /* Config */
        (4), /* Baudrate */
        (0), /* CsChannelId */
        (SPI_NSS), /* CS Identifier */
        (&(Spi_PhyHwUnits[SPI_PHYUNIT_SPI1])), /* Hw Unit */
        (0), /* Time Clk 2 Cs */
    },
};



P2VAR(Spi_ChannelConfigType, SPI_VAR, SPI_APPL_CONST) Spi_JobNFC_JOBChannels[] = {
    &Spi_ChannelConfigs[SPI_CHANNEL_EB_BUFFER],
};



VAR(Spi_JobConfigType, SPI_VAR) Spi_JobConfigs[] = {
    /* SPI_JOB_NFC_JOB*/
    {
        (1), /* Channel Number */
        (1), /* Hw Unit Synchronous */
        (0), /* Job Priority */
        0, /* Current Channel */
        SPI_JOB_OK, /* Job Status */
        &Spi_DeviceConfigs[SPI_DEVICE_NFC], /* Job Device Config */
        Spi_JobNFC_JOBChannels, /* Job's channels*/
        NULL_PTR, /* Next job pointer */
        NULL_PTR /* Sequence End Notification */
    },
};



P2VAR(Spi_JobConfigType, SPI_VAR, SPI_APPL_CONST) Spi_SequenceNFC_SEQJobs[] = {
    &Spi_JobConfigs[0]
};



VAR(Spi_SequenceConfigType, SPI_VAR) Spi_SequenceConfigs[] = {
    /* SPI_SEQUENCE_NFC_SEQ*/
    {
        SPI_SEQ_OK, 
        (1),
        Spi_SequenceNFC_SEQJobs, 
        0, /* Spi Remain Job */
        NULL_PTR /* Sequence End Notification */
    },
};



VAR(Spi_ConfigType, SPI_VAR) Spi_PBCfgVariantPredefined = {
    1, /* Number device */
    1, /* Number channel */
    1, /* Number job */
    1, /* Number channel */
    Spi_ChannelConfigs,
    Spi_JobConfigs,
    Spi_SequenceConfigs,
    Spi_DeviceConfigs,

};

#define SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "Spi_MemMap.h"

/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL FUNCTIONS
==================================================================================================*/
#define SPI_START_SEC_CODE
#include "Spi_MemMap.h"


/*==================================================================================================
*                                      GLOBAL FUNCTIONS
==================================================================================================*/



#define SPI_STOP_SEC_CODE
#include "Spi_MemMap.h"

#endif /* (STD_ON == SPI_PRECOMPILE_SUPPORT) */

#ifdef __cplusplus
}
#endif


/** @} */

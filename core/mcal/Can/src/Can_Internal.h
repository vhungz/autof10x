/*-------------------------------- Arctic Core ------------------------------
 * Copyright (C) 2013, ArcCore AB, Sweden, www.arccore.com.
 * Contact: <contact@arccore.com>
 *
 * You may ONLY use this file:
 * 1)if you have a valid commercial ArcCore license and then in accordance with
 * the terms contained in the written license agreement between you and ArcCore,
 * or alternatively
 * 2)if you follow the terms found in GNU General Public License version 2 as
 * published by the Free Software Foundation and appearing in the file
 * LICENSE.GPL included in the packaging of this file or here
 * <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>
 *-------------------------------- Arctic Core -----------------------------*/
/** @tagSettings DEFAULT_ARCHITECTURE=TMS570|ZYNQ|MPC5777M|PPC|MPC5607B|MPC5645S|MPC5748G|MPC5747C|STM32|JACINTO5|JACINTO6 */
/** @reqSettings DEFAULT_SPECIFICATION_REVISION=4.1.2 */


/** @file Can_Internal.h
 *  This files contains the hardware specific API definitions.
 */
/** \addtogroup Mcal
  * @brief This is the major Mcal folder
  *
  *  This group is for the Mcal
  *  @{
  */

 /** \addtogroup Can
  *  This is the Mcal Can Group
  *  @{
  */
#ifndef CAN_INTERNAL_H_
#define CAN_INTERNAL_H_

#include "Can_Cfg.h"

#if !defined(CFG_TMS570)
/**
 * Deinit the CAN driver (added by ArcCore and not a part of the standard)
 */
void Can_Arc_DeInit(void);
void Can_Arc_Hw_DeInit(void);
#endif	/* !defined(CFG_TMS570) */

#if defined(CFG_MPC5777M) || defined(CFG_STM32) || defined(CFG_PPC) || defined(CFG_MPC5607B) || defined(CFG_MPC5645S) || defined(CFG_MPC5748G) || defined(CFG_MPC5747C)

#if defined(CFG_CAN_TEST)

typedef struct {
    uint64_t mbMaskTx;
    uint64_t mbMaskRx;
} Can_TestType;

/**
 * Returns test info
 */
Can_TestType * Can_Arc_GetTestInfo( void );


#endif  /* defined(CFG_CAN_TEST) */
#endif	/* defined(CFG_MPC5777M)... */



#if !defined(CFG_ZYNQ) && !defined(CFG_RH850) && !defined(CFG_STM32)
/** @brief Hardware specific implementation of Can_DisableControllerInterrupts. Does not perform any other logic like changing the disable interrupt count.
 *  @param[in] Controller - CAN controller id, for which the interrupts shall be disabled.
 */
void Can_Hw_DisableControllerInterrupts( uint8 Controller );

/** @brief Hardware specific implementation of Can_EnableControllerInterrupts
 * Does not perform any other logic like changing the disable interrupt count.
 *  @param[in] Controller - CAN controller id, for which the interrupts shall be enabled.
 */
void Can_Hw_EnableControllerInterrupts( uint8 Controller );
#	if !defined(CFG_TMS570)
void Can_Hw_Init(void);
#	endif

#		if defined(CFG_TMS570)
/** @brief Hardware specific implementation of Can_Init
 *  @param[in] Config - Pointer to configuration set
 */
void Can_Hw_Init(const Can_ConfigType *Config);

/** @brief Hardware specific implementation of Can_DeInit
 */
void Can_Hw_DeInit(void);

/**
 * @brief Hardware specific implementation of Can_SetControllerMode
 * @param[in] Controller - CAN controller id, for which the status shall be changed
 * @param[in] Transition - Transition value to request new CAN controller state
 * @return Can_ReturnType.
 * @retval CAN_OK: request accepted
 * @retval CAN_NOT_OK: request not accepted, a development error occurred
 */
Can_ReturnType Can_Hw_SetControllerMode( uint8 Controller, Can_StateTransitionType Transition );

/**
 * @brief Hardware specific implementation of Can_Write
 * @param[in] Hth - information which HW-transmit handle shall be used for transmit. Implicitly this is also the information about the controller to use because the Hth numbers are unique inside one hardware unit.
 * @param[in] PduInfo - Pointer to SDU user memory, Data Length and Identifier.
 * @return Can_ReturnType
 * @retval CAN_OK: Write command has been accepted
 * @retval CAN_NOT_OK: Development error occurred
 * @retval CAN_BUSY: No TX hardware buffer available or pre-emptive call of Can_Write that can't be implemented re-entrant
 */
Can_ReturnType Can_Hw_Write( Can_HwHandleType Hth, const Can_PduType *PduInfo );

/**
 * @brief Hardware specific implementation of Can_CheckWakeup
 * @param[in] Controller - CAN controller id, for which the wakeup shall be checked
 * @return Can_ReturnType.
 * @retval CAN_OK: request accepted
 * @retval CAN_NOT_OK: request not accepted, a development error occurred
 */
Can_ReturnType Can_Hw_CheckWakeup( uint8 Controller );

/**
 * @brief Hardware specific implementation of Can_SetBaudrate
 * @param[in] Controller - CAN controller id, whose baud rate shall be set
 * @param[in] BaudRateConfigID - in kpbs, references a baud rate configuration by ID
 * @return Std_ReturnType.
 * @retval E_OK: Service request accepted, setting of (new) baud rate started
 * @retval E_NOT_OK: Service request not accepted
 */
Std_ReturnType Can_Hw_SetBaudrate( uint8 Controller, uint16 BaudRateConfigID );




/**
 * @brief Arccore specified function. Return the driver state.
 * @return Can_DriverStateType.
 * @retval CAN_UNINIT: Can driver is not initialized.
 * @retval CAN_READY: Can driver is initialized and ready to be used.
 */
Can_DriverStateType Can_Hw_GetDriverState(void);

/**
 * @brief Arccore specified function. Return a controller handle in the Can unit.
 * @param[in] Controller - CAN controller id, for which the handle is returned.
 * @return Can_UnitType pointer.
 * @retval A controller handle, it is a structure.
 */
Can_UnitType* const get_private_data(uint8 Controller);

/**
 * @brief Arccore specified function. Return the number of calling of Can_DisableControllerInterrupts().
 * @param[in] Controller - CAN controller id, for which the summary data is returned.
 * @return uint32.
 * @retval Number of calling times of Can_DisableControllerInterrupts function.
 */
uint32 Can_Hw_GetDisableCountNum(uint8 Controller);

/**
 * @brief Arccore specified function. Set the number of calling of Can_DisableControllerInterrupts().
 * @param[in] Controller - CAN controller id, for which the summary data is set.
 * @param[in] val - Number to be set.
 */
void Can_Hw_SetDisableCountNum(uint8 Controller, uint32 val);

/**
 * @brief Arccore specified function. Decrement the number of calling of Can_DisableControllerInterrupts() by one.
 * @param[in] Controller - CAN controller id, for which the number is decremented.
 */
void Can_Hw_DecDisableCountNum(uint8 Controller);

/**
 * @brief Arccore specified function. Increment the number of calling of Can_DisableControllerInterrupts() by one.
 * @param[in] Controller - CAN controller id, for which the number is incremented.
 */
void Can_Hw_IncDisableCountNum(uint8 Controller);

/**
 * @brief Arccore specified function. This function will handle fifo buffers as well as individual message rx objects.
 * Note that fifos must use consecutive message objects.
 * @param[in] MsgObjectNr - message object number, it is used to access message object in hardware.
 * @param[in] controllerConfig - specific configuration set of a controller.
 * @param[in] hrh - Represents the hardware object handles of a CAN controller.
 */
void Can_HandleRxOk(uint8 MsgObjectNr, const Can_ControllerConfigType * controllerConfig, const Can_HardwareObjectType * hrh);

/**
 * @brief Arccore specified function. Handle the interrupt of a certain controller.
 * @param[in] controllerNr - CAN controller id.
 */
void Can_InterruptHandler(CanControllerIdType controllerNr);

/**
 * @brief Arccore specified function. Initialize the hardware register of a certain controller. It is called by Can_Hw_Init.
 * @param[in] controllerConfig - Pointer to controller configuration.
 */
void Can_InitHardwareObjects( const Can_ControllerConfigType * controllerConfig);

/**
 * @brief Arccore specified function.
 * @param[in] ControllerId - CAN controller id.
 * @param[in] IfRegId - register index within Can interface register set in TMS570.
 * @return Can_ReturnType.
 * @retval CAN_OK: no error occurs while waiting.
 * @retval CAN_NOT_OK: error occurs while waiting, timeout.
 */
Can_ReturnType dcan_wait_until_not_busy(uint8 ControllerId, uint32 IfRegId);

/**
 * @brief Arccore specified function.
 * @param[in] ControllerId - CAN controller id.
 * @param[in] IfRegId - register index within Can interface register set in TMS570.
 */
void dcan_wait_until_not_busy_no_rv(uint8 ControllerId, uint32 IfRegId);

/**
 * @brief Arccore specified function. Gets the index of a controller in the global configuration structure.
 * @param[in] controller - CAN controller id
 * @return uint8.
 * @retval Config index.
 */
uint8 getControllerConfigIdx(uint8 controller);

/**
 * @brief Arccore specified function. Return configuration of a controller.
 * @param[in] _controllerID - CAN controller id
 * @return Can_ControllerConfigType pointer.
 * @retval Config structure of a controller.
 */
const Can_ControllerConfigType* GET_CONTROLLER_CONFIG(uint8 _controllerID);

/**
 * @brief Arccore specified function. When interrupt error occurs, increment the counter.
 */
void Can_HandleInterruptError(void);

/**
 * @brief Arccore specified function. Handle busoff state, switch the controller state.
 * @param[in] controllerNr - CAN controller id
 */
void Can_HandleBusOff(CanControllerIdType controllerNr);

/**
 * @brief Arccore specified function. Return hardware handle of a unit.
 * @param[in] handle - Represents the hardware object handle number of a CAN hardware unit.
 * @return Can_HardwareObjectType pointer.
 * @retval Hardware object handle.
 */
const Can_HardwareObjectType * Can_FindHoh(Can_HwHandleType handle);

/**
 * @brief Arccore specified function. Return hardware handle of a unit.
 * @param[in] controllerNr - Can controller Id.
 * @param[in] MsgNr - Message Id.
 */
void Can_HandleTxOk(CanControllerIdType controllerNr, uint8 MsgNr);

/**
 * @brief Arccore specified function. Install interrupt handlers to OS.
 * @param[in] ctrl - Can controller config data pointer.
 */
void Can_InstallInterruptHandlers(const Can_ControllerConfigType * ctrl);

/**
 * @brief Arccore specified function. 64-bit integer log2.
 * @param[in] val - input value to the function.
 * @return uint8.
 * @retval log2 of val.
 */
uint8 ilog2_64(uint64 val);


#		elif defined(CFG_JACINTO)
void Can_Hw_DeInit(void);
void Can_HandleRxOk(uint8 MsgObjectNr, const Can_ControllerConfigType * controllerConfig, const Can_HardwareObjectType * hrh);
void Can_InterruptHandler(CanControllerIdType controllerNr);
void Can_InitHardwareObjects( const Can_ControllerConfigType * controllerConfig);
Can_ReturnType dcan_wait_until_not_busy(uint8 ControllerId, uint32 IfRegId);
void dcan_wait_until_not_busy_no_rv(uint8 ControllerId, uint32 IfRegId);
uint8 getControllerConfigIdx(uint8 controller);
const Can_ControllerConfigType* GET_CONTROLLER_CONFIG(uint8 configId);
void Can_HandleInterruptError(void);
void Can_HandleBusOff(CanControllerIdType controllerNr);
const Can_HardwareObjectType * Can_FindHoh(Can_HwHandleType handle);
void Can_HandleTxOk(CanControllerIdType controllerNr, uint8 MsgNr);
uint8 ilog2_64(uint64 val);



#		endif	/*defined(CFG_TMS570LS12X)*/


#endif	/*!defined(CFG_ZYNQ)*/


#ifdef HOST_TEST
/**
 * Checks that the module is in the state CAN_UNINIT.
 *
 * @return E_OK if module is in state CAN_UNINIT, else E_NOT_OK.
 */
Std_ReturnType Can_Test_DriverStateIsUninit(void);

/**
 * Checks that the module is in the state CAN_READY.
 *
 * @return E_OK if module is in state CAN_READY, else E_NOT_OK.
 */
Std_ReturnType Can_Test_DriverStateIsReady(void);

/**
 * Checks that all hardware units in the module are in the state state.
 *
 * Cannot be called before Can_Init.
 *
 * @param state The state the hardware units are expected to be in.
 * @return E_OK if all units are in the expected state, else E_NOT_OK.
 */
Std_ReturnType Can_Test_AllUnitsInState(CanIf_ControllerModeType state);

Can_HardwareObjectType * getCanHwObject(uint8 controller, uint8 index);
#endif /* HOST_TEST */


#endif
/** @}*/
/** @}*/

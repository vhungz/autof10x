/** 
*   @file           StdRegMacros.h
*   @version        0.9.0
*   @brief          AUTOSAR {M4_MODULE_NAME_CAP} - <TODO>
*
*   @details        The file Compiler.h provides macros for the encapsulation of definitions and
*                   declarations.
*                   This file contains sample code only. It is not part of the production code 
*                   deliverables
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup BASE_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/

/**
* @file         Compiler.h
* @requirements COMPILER047
*/
#ifndef STDREGMACROS_H
#define STDREGMACROS_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief  Parameters that shall be published within the compiler abstraction header file and also in
          the module's description file.
@{
*/
#define STDREGMACROS_H_VENDOR_ID                             1
#define STDREGMACROS_H_AR_RELEASE_MAJOR_VERSION              4
#define STDREGMACROS_H_AR_RELEASE_MINOR_VERSION              3
#define STDREGMACROS_H_AR_RELEASE_REVISION_VERSION           1
#define STDREGMACROS_H_SW_MAJOR_VERSION                      0
#define STDREGMACROS_H_SW_MINOR_VERSION                      9
#define STDREGMACROS_H_SW_PATCH_VERSION                      0


/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/

/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/

#define BIT_0                                   ((uint8)  0x0001U)
#define BIT_1                                   ((uint8)  0x0002U)
#define BIT_2                                   ((uint8)  0x0004U)
#define BIT_3                                   ((uint8)  0x0008U)
#define BIT_4                                   ((uint8)  0x0010U)
#define BIT_5                                   ((uint8)  0x0020U)
#define BIT_6                                   ((uint8)  0x0040U)
#define BIT_7                                   ((uint8)  0x0080U)
#define BIT_8                                   ((uint16) 0x0100U)
#define BIT_9                                   ((uint16) 0x0200U)
#define BIT_10                                  ((uint16) 0x0400U)
#define BIT_11                                  ((uint16) 0x0800U)
#define BIT_12                                  ((uint16) 0x1000U)
#define BIT_13                                  ((uint16) 0x2000U)
#define BIT_14                                  ((uint16) 0x4000U)
#define BIT_15                                  ((uint16) 0x8000U)

#define BIT_16                                  ((uint32) 0x00010000U)
#define BIT_17                                  ((uint32) 0x00020000U)
#define BIT_18                                  ((uint32) 0x00040000U)
#define BIT_19                                  ((uint32) 0x00080000U)
#define BIT_20                                  ((uint32) 0x00100000U)
#define BIT_21                                  ((uint32) 0x00200000U)
#define BIT_22                                  ((uint32) 0x00400000U)
#define BIT_23                                  ((uint32) 0x00800000U)
#define BIT_24                                  ((uint32) 0x01000000U)
#define BIT_25                                  ((uint32) 0x02000000U)
#define BIT_26                                  ((uint32) 0x04000000U)
#define BIT_27                                  ((uint32) 0x08000000U)
#define BIT_28                                  ((uint32) 0x10000000U)
#define BIT_29                                  ((uint32) 0x20000000U)
#define BIT_30                                  ((uint32) 0x40000000U)
#define BIT_31                                  ((uint32) 0x80000000U)


#define BIT_0_INDEX                             ((uint8) 0U)
#define BIT_1_INDEX                             ((uint8) 1U)
#define BIT_2_INDEX                             ((uint8) 2U)
#define BIT_3_INDEX                             ((uint8) 3U)
#define BIT_4_INDEX                             ((uint8) 4U)
#define BIT_5_INDEX                             ((uint8) 5U)
#define BIT_6_INDEX                             ((uint8) 6U)
#define BIT_7_INDEX                             ((uint8) 7U)
#define BIT_8_INDEX                             ((uint8) 8U)
#define BIT_9_INDEX                             ((uint8) 9U)
#define BIT_10_INDEX                            ((uint8) 10U)
#define BIT_11_INDEX                            ((uint8) 11U)
#define BIT_12_INDEX                            ((uint8) 12U)
#define BIT_13_INDEX                            ((uint8) 13U)
#define BIT_14_INDEX                            ((uint8) 14U)
#define BIT_15_INDEX                            ((uint8) 15U)

#define BIT_16_INDEX                            ((uint8) 16U)
#define BIT_17_INDEX                            ((uint8) 17U)
#define BIT_18_INDEX                            ((uint8) 18U)
#define BIT_19_INDEX                            ((uint8) 19U)
#define BIT_20_INDEX                            ((uint8) 20U)
#define BIT_21_INDEX                            ((uint8) 21U)
#define BIT_22_INDEX                            ((uint8) 22U)
#define BIT_23_INDEX                            ((uint8) 23U)
#define BIT_24_INDEX                            ((uint8) 24U)
#define BIT_25_INDEX                            ((uint8) 25U)
#define BIT_26_INDEX                            ((uint8) 26U)
#define BIT_27_INDEX                            ((uint8) 27U)
#define BIT_28_INDEX                            ((uint8) 28U)
#define BIT_29_INDEX                            ((uint8) 23U)
#define BIT_30_INDEX                            ((uint8) 30U)
#define BIT_31_INDEX                            ((uint8) 31U)

/**
* @brief 8 bits memory write macro
*/
#define REG_WRITE8(address, value)        ((*(volatile uint8*)(address))=  (value))
/**
* @brief 16 bits memory write macro.
*/
#define REG_WRITE16(address, value)       ((*(volatile uint16*)(address))= (value))
/**
* @brief 32 bits memory write macro.
*/
#define REG_WRITE32(address, value)       ((*(volatile uint32*)(address))= (value))


/**
* @brief 8 bits memory read macro.
*/
#define REG_READ8(address)                (*(volatile uint8*)(address))
/**
* @brief 16 bits memory read macro.
* @violates @ref StdRegMacros_h_REF_1 MISRA 2004 Advisory Rule 19.7, A function should be used in
* preference to a function-like macro.
*/
#define REG_READ16(address)               (*(volatile uint16*)(address))
/**
* @brief 32 bits memory read macro.
*/
#define REG_READ32(address)               (*(volatile uint32*)(address))

/**
* @brief 8 bits indexed memory write macro. Index i must have the data type uint32.
*/
#define REG_AWRITE8(address, i, value)     (REG_WRITE8 ((address)+(i), (value)))

/**
* @brief 16 bits indexed memory write macro. Index i must have the data type uint32.
*/
#define REG_AWRITE16(address, i, value)    (REG_WRITE16((address)+((uint32)((i)<<1U)), (value)))
/**
* @brief 32 bits indexed memory write macro. Index i must have the data type uint32.
*/
#define REG_AWRITE32(address, i, value)    (REG_WRITE32((address)+((uint32)((i)<<2U)), (value)))


/**
* @brief 8 bits indexed memory read macro. Index i must have the data type uint32.
*/
#define REG_AREAD8(address, i )            (REG_READ8 ((address)+(i)))
/**
* @brief 16 bits indexed memory read macro. Index i must have the data type uint32.
* @violates @ref StdRegMacros_h_REF_1 MISRA 2004 Advisory Rule 19.7, A function should be used in
* preference to a function-like macro.
*/
#define REG_AREAD16(address, i)            (REG_READ16((address)+((uint32)((i)<<1U))))
/**
* @brief 32 bits indexed memory read macro. Index i must have the data type uint32.
*/
#define REG_AREAD32(address, i)            (REG_READ32((address)+((uint32)((i)<<2U))))


/**
* @brief 8 bits bits clearing macro.
* @violates @ref StdRegMacros_h_REF_1 MISRA 2004 Advisory Rule 19.7, A function should be used in
* preference to a function-like macro.
*/
#define REG_BIT_CLEAR8(address, mask)     ((*(volatile uint8*)(address))&= (~(mask)))
/**
* @brief 16 bits bits clearing macro.
* preference to a function-like macro.
*/
#define REG_BIT_CLEAR16(address, mask)    ((*(volatile uint16*)(address))&= (~(mask)))
/**
* @brief 32 bits bits clearing macro.
*/
#define REG_BIT_CLEAR32(address, mask)    ((*(volatile uint32*)(address))&= (~(mask)))


/**
* @brief 8 bits bits getting macro.
*/
#define REG_BIT_GET8(address, mask)       ((*(volatile uint8*)(address))& (mask))
/**
* @brief 16 bits bits getting macro.
* @violates @ref StdRegMacros_h_REF_1 MISRA 2004 Advisory Rule 19.7, A function should be used in
* preference to a function-like macro.
*/
#define REG_BIT_GET16(address, mask)      ((*(volatile uint16*)(address))& (mask))
/**
* @brief 32 bits bits getting macro.
*/
#define REG_BIT_GET32(address, mask)      ((*(volatile uint32*)(address))& (mask))


/**
* @brief 8 bits bits getting macro.
*/
#define REG_BIT_IS_SET8(address, mask)       (((uint8)(*(volatile uint8*)(address))& (mask)) == mask)
/**
* @brief 16 bits bits getting macro.
* @violates @ref StdRegMacros_h_REF_1 MISRA 2004 Advisory Rule 19.7, A function should be used in
* preference to a function-like macro.
*/
#define REG_BIT_IS_SET16(address, mask)      (((uint16)(*(volatile uint16*)(address))& (mask)) == mask)
/**
* @brief 32 bits bits getting macro.
*/
#define REG_BIT_IS_SET32(address, mask)      (((uint32)(*(volatile uint32*)(address)) & (uint32) (mask)) == (uint32) mask)


/**
* @brief 8 bits bits setting macro.
*/
#define REG_BIT_SET8(address, mask)       ((*(volatile uint8*)(address))|= (mask))
/**
* @brief 16 bits bits setting macro.
*/
#define REG_BIT_SET16(address, mask)      ((*(volatile uint16*)(address))|= (mask))
/**
* @brief 32 bits bits setting macro.
*/
#define REG_BIT_SET32(address, mask)      ((*(volatile uint32*)(address))|= (mask))


/**
* @brief 8 bit clear bits and set with new value
* @note In the current implementation, it is caller's (user's) responsability to make sure that
*       value has only "mask" bits set - (value&~mask)==0
*/
#define REG_RMW8(address, mask, value)    (REG_WRITE8((address), ((REG_READ8(address)& ((uint8)~(mask)))| (value))))
/**
* @brief 16 bit clear bits and set with new value
* @note In the current implementation, it is caller's (user's) responsability to make sure that
*       value has only "mask" bits set - (value&~mask)==0
*/
#define REG_RMW16(address, mask, value)   (REG_WRITE16((address), ((REG_READ16(address)& ((uint16)~(mask)))| (value))))
/**
* @brief 32 bit clear bits and set with new value
* @note In the current implementation, it is caller's (user's) responsability to make sure that
*       value has only "mask" bits set - (value&~mask)==0
*/
#define REG_RMW32(address, mask, value)   (REG_WRITE32((address), ((REG_READ32(address)& ((uint32)~(mask)))| (value))))


/*==================================================================================================
*                                             ENUMS
==================================================================================================*/

/*==================================================================================================
*                                 STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/

/*==================================================================================================
*                                 GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/

/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /* #ifndef STDREGMACROS_H */

/** @} */

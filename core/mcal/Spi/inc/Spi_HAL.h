/** 
*   @file           Spi_HAL.h
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/
#ifndef SPI_HAL_H
#define SPI_HAL_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Std_Types.h"
#include "Spi_Cfg.h"
#include "Spi_REG.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Mcl driver header file and also in the
*        module's description file
*/
#define SPI_HAL_H_VENDOR_ID                             1
#define SPI_HAL_H_AR_RELEASE_MAJOR_VERSION              4
#define SPI_HAL_H_AR_RELEASE_MINOR_VERSION              3
#define SPI_HAL_H_AR_RELEASE_REVISION_VERSION           1
#define SPI_HAL_H_SW_MAJOR_VERSION                      0
#define SPI_HAL_H_SW_MINOR_VERSION                      9
#define SPI_HAL_H_SW_PATCH_VERSION                      0



/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
///* Check if Spi_HAL.h and Std_Types.h file are of the same Autosar version */
//#if ((SPI_HAL_H_AR_RELEASE_MAJOR_VERSION    != STD_TYPES_H_AR_RELEASE_MAJOR_VERSION) || \
//     (SPI_HAL_H_AR_RELEASE_MINOR_VERSION    != STD_TYPES_H_AR_RELEASE_MINOR_VERSION) || \
//     (SPI_HAL_H_AR_RELEASE_REVISION_VERSION != STD_TYPES_H_AR_RELEASE_REVISION_VERSION) \
//    )
//    #error "AutoSar Version Numbers of Spi_HAL.h and Std_Types.h are different"
//#endif
///* Check if Spi_HAL.h and Std_Types.h header file are of the same Software version */
//#if ((SPI_HAL_H_SW_MAJOR_VERSION != STD_TYPES_H_SW_MAJOR_VERSION) || \
//     (SPI_HAL_H_SW_MINOR_VERSION != STD_TYPES_H_SW_MINOR_VERSION) || \
//     (SPI_HAL_H_SW_PATCH_VERSION != STD_TYPES_H_SW_PATCH_VERSION)    \
//    )
//    #error "Software Version Numbers of Spi_HAL.h and Std_Types.h are different"
//#endif

/* Check if Spi_HAL.h and Spi_Cfg.h file are of the same Autosar version */
#if ((SPI_HAL_H_AR_RELEASE_MAJOR_VERSION    != SPI_CFG_H_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_HAL_H_AR_RELEASE_MINOR_VERSION    != SPI_CFG_H_AR_RELEASE_MINOR_VERSION) || \
     (SPI_HAL_H_AR_RELEASE_REVISION_VERSION != SPI_CFG_H_AR_RELEASE_REVISION_VERSION) \
    )
    #error "AutoSar Version Numbers of Spi_HAL.h and Spi_Cfg.h are different"
#endif
/* Check if Spi_HAL.h and Spi_Cfg.h header file are of the same Software version */
#if ((SPI_HAL_H_SW_MAJOR_VERSION != SPI_CFG_H_SW_MAJOR_VERSION) || \
     (SPI_HAL_H_SW_MINOR_VERSION != SPI_CFG_H_SW_MINOR_VERSION) || \
     (SPI_HAL_H_SW_PATCH_VERSION != SPI_CFG_H_SW_PATCH_VERSION)    \
    )
    #error "Software Version Numbers of Spi_HAL.h and Spi_Cfg.h are different"
#endif




/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/



/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/
#define Spi_HAL_HWInit(config)                  Spi_REG_HWInit(config)
#define Spi_HAL_SyncTransmit(device, channel)   Spi_REG_SyncTransmit(device, channel)
/*==================================================================================================
*                                              ENUMS
==================================================================================================*/


/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/


/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/


/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/
/* Allocate defined section for SPI code */
#define SPI_START_SEC_CODE
#include "Spi_MemMap.h"

FUNC (void, SPI_CODE) Spi_HAL_AsyncTransmit(
    P2VAR(Spi_JobConfigType, SPI_VAR, SPI_APPL_CONST) pJob
);
FUNC (void, SPI_CODE) Spi_HAL_ChannelComplete(
    P2VAR(Spi_JobConfigType, SPI_VAR, SPI_APPL_CONST) pJob
);


#define SPI_STOP_SEC_CODE
#include "Spi_MemMap.h"

#ifdef __cplusplus
}
#endif

#endif /* #ifndef SPI_HAL_H */

/** @} */

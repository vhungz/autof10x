/** 
*   @file           Spi_MemMap.h
*   @version        0.9.0
*   @brief          AUTOSAR {M4_MODULE_NAME_CAP} - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup BASE_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
//#include "CompilerDefinition.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @{
* @brief Parameters that shall be published within the memory map header file and also in the
*       module's description file
*/
#define SPI_MEMMAP_H_VENDOR_ID                             1
#define SPI_MEMMAP_H_AR_RELEASE_MAJOR_VERSION              4
#define SPI_MEMMAP_H_AR_RELEASE_MINOR_VERSION              3
#define SPI_MEMMAP_H_AR_RELEASE_REVISION_VERSION           1
#define SPI_MEMMAP_H_SW_MAJOR_VERSION                      0
#define SPI_MEMMAP_H_SW_MINOR_VERSION                      9
#define SPI_MEMMAP_H_SW_PATCH_VERSION                      0


/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
                                           CONSTANTS
==================================================================================================*/

/*==================================================================================================
                                       DEFINES AND MACROS
==================================================================================================*/


/**
* @brief Symbol used for checking correctness of the includes
*/
#define MEMMAP_ERROR

/**************************************************************************************************/
/********************************************* GREENHILLS *****************************************/
/**************************************************************************************************/

/**************************************************************************************************/
/********************************************* Linaro *********************************************/
/**************************************************************************************************/
#if defined(_LINARO_C_STM32F10X_)
/**************************************** SPI *******************************/
#ifdef SPI_START_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif


#ifdef SPI_START_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif


#ifdef SPI_START_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif
/**************************************************************************************************/
/********************************************* DS5 ************************************************/
/**************************************************************************************************/
#elif defined(_ARM_DS5_C_STM32F10X_)
/**************************************** SPI *******************************/
#ifdef SPI_START_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rodata=".mcal_const_cfg"
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rodata=".mcal_const_cfg"
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rodata=".mcal_const_cfg"
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rodata=".mcal_const_cfg"
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rodata=".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rodata=".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rodata=".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rodata=".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rodata=".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section code=".mcal_text"
#endif

#ifdef SPI_STOP_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section code=".ramcode"
#endif

#ifdef SPI_STOP_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section code=".acspi_code_rom"
#endif

#ifdef SPI_STOP_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section zidata=".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section zidata=".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif


#ifdef SPI_START_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section zidata=".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section zidata=".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section zidata=".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section zidata=".mcal_bss_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rwdata=".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rwdata=".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rwdata=".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rwdata=".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    #pragma arm section rwdata=".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif


#ifdef SPI_START_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_START_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
     #undef MEMMAP_ERROR
    /* no definition -> default compiler settings are used */
#endif




/**************************************************************************************************/
/********************************************* IAR ************************************************/
/**************************************************************************************************/
#elif defined(_IAR_C_STM32F10X_)
/**************************************** SPI *******************************/
#ifdef SPI_START_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_const_cfg"
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif

#ifdef SPI_START_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_const_cfg"
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif

#ifdef SPI_START_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_const_cfg"
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif

#ifdef SPI_START_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_const_cfg"
#endif

#ifdef SPI_STOP_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONFIG_DATA_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif

#ifdef SPI_START_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif

#ifdef SPI_START_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif

#ifdef SPI_START_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif

#ifdef SPI_START_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif

#ifdef SPI_START_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_const"
#endif

#ifdef SPI_STOP_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CONST_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif

#ifdef SPI_START_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_function_attributes = @ ".mcal_text"
#endif

#ifdef SPI_STOP_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_function_attributes =
#endif

#ifdef SPI_START_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_function_attributes = @ ".ramcode"

#endif

#ifdef SPI_STOP_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_RAMCODE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_function_attributes =
    
#endif

#ifdef SPI_START_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_function_attributes = @ ".acspi_code_rom"

#endif

#ifdef SPI_STOP_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_CODE_AC
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_function_attributes = 
    
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_BOOLEAN
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_8
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_16
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_32
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_data"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif


#ifdef SPI_START_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_bss_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_NO_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = @ ".mcal_data_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_BOOLEAN_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
   #pragma default_variable_attributes = @ ".mcal_data_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_8_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
   #pragma default_variable_attributes = @ ".mcal_data_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_16_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
   #pragma default_variable_attributes = @ ".mcal_data_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_32_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes =
#endif

#ifdef SPI_START_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_START_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
   #pragma default_variable_attributes = @ ".mcal_data_no_cacheable"
#endif

#ifdef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef SPI_STOP_SEC_VAR_INIT_UNSPECIFIED_NO_CACHEABLE
    /**
    * @file Spi_MemMap.h
    * @violates @ref Spi_MemMap_h_REF_1 MISRA 2004 Required Rule 19.6, use of '#undef' is discouraged
    */
    #undef MEMMAP_ERROR
    #pragma default_variable_attributes = 
#endif
#endif 
#undef MEMMAP_ERROR
/**************************************************************************************************/
/****************************************** Report error ******************************************/
/**************************************************************************************************/
#ifdef MEMMAP_ERROR
    #error "MemMap.h, no valid memory mapping symbol defined."
#endif
                                                                                                     
/*================================================================================================== 
*                                            ENUMS                                                   
==================================================================================================*/ 
                                                                                                     
/*================================================================================================== 
*                                 STRUCTURES AND OTHER TYPEDEFS                                      
==================================================================================================*/ 
                                                                                                     
/*================================================================================================== 
*                                 GLOBAL VARIABLE DECLARATIONS                                       
==================================================================================================*/ 
                                                                                                     
/*================================================================================================== 
*                                     FUNCTION PROTOTYPES                                            
==================================================================================================*/ 
                                                                                                     
#ifdef __cplusplus                                                                                   
}                                                                                                    
#endif                                                                                               

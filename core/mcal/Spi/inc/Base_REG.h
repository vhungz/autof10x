/** 
*   @file           Base_REG.h
*   @version        0.9.0
*   @brief          AUTOSAR {M4_MODULE_NAME_CAP} - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup BASE_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/
#ifndef BASE_REG_H
#define BASE_REG_H

/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Port driver header file and also in the
*        module's description file
*/
#define BASE_REG_H_VENDOR_ID                             1
#define BASE_REG_H_AR_RELEASE_MAJOR_VERSION              4
#define BASE_REG_H_AR_RELEASE_MINOR_VERSION              3
#define BASE_REG_H_AR_RELEASE_REVISION_VERSION           1
#define BASE_REG_H_SW_MAJOR_VERSION                      0
#define BASE_REG_H_SW_MINOR_VERSION                      9
#define BASE_REG_H_SW_PATCH_VERSION                      0


/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/

#define WaitForInterrupt()                      __asm volatile("wfi;")
#define WaitForEvent()                          __asm volatile("wfe;")


#define PORT_PIN_0                              ((uint8) 0U)
#define PORT_PIN_1                              ((uint8) 1U)
#define PORT_PIN_2                              ((uint8) 2U)
#define PORT_PIN_3                              ((uint8) 3U)
#define PORT_PIN_4                              ((uint8) 4U)
#define PORT_PIN_5                              ((uint8) 5U)
#define PORT_PIN_6                              ((uint8) 6U)
#define PORT_PIN_7                              ((uint8) 7U)
#define PORT_PIN_8                              ((uint8) 8U)
#define PORT_PIN_9                              ((uint8) 9U)
#define PORT_PIN_10                             ((uint8) 10U)
#define PORT_PIN_11                             ((uint8) 11U)
#define PORT_PIN_12                             ((uint8) 12U)
#define PORT_PIN_13                             ((uint8) 13U)
#define PORT_PIN_14                             ((uint8) 14U)
#define PORT_PIN_15                             ((uint8) 15U)


#define TIM2_BASEADDRESS                        (((uint32)0x40000000UL))
#define TIM3_BASEADDRESS                        (((uint32)0x40000400UL))
#define TIM4_BASEADDRESS                        (((uint32)0x40000800UL))
#define RTC_BASEADDRESS                         (((uint32)0x40002800UL))
#define WWDG_BASEADDRESS                        (((uint32)0x40002C00UL))
#define IWDG_BASEADDRESS                        (((uint32)0x40003000UL))
#define SPI2_BASEADDRESS                        (((uint32)0x40003800UL))
#define USART2_BASEADDRESS                      (((uint32)0x40004400UL))
#define USART3_BASEADDRESS                      (((uint32)0x40004800UL))
#define I2C1_BASEADDRESS                        (((uint32)0x40005400UL))
#define I2C2_BASEADDRESS                        (((uint32)0x40005800UL))
#define USB_BASEADDRESS                         (((uint32)0x40005C00UL))
#define USB_RAM_BASEADDRESS                     (((uint32)0x40006000UL))
#define CAN_RAM_BASEADDRESS                     (((uint32)0x40006000UL))
#define BXCAN_BASEADDRESS                       (((uint32)0x40006800UL))
#define BKP_BASEADDRESS                         (((uint32)0x40006C00UL))
#define PWR_BASEADDRESS                         (((uint32)0x40007000UL))
#define AFIO_BASEADDRESS                        (((uint32)0x40010000UL))
#define EXTI_BASEADDRESS                        (((uint32)0x40010400UL))

#define PORT_A_BASEADDRESS                      (((uint32)0x40010800UL))
#define PORT_B_BASEADDRESS                      (((uint32)0x40010C00UL))
#define PORT_C_BASEADDRESS                      (((uint32)0x40011000UL))
#define PORT_D_BASEADDRESS                      (((uint32)0x40011400UL))
#define PORT_E_BASEADDRESS                      (((uint32)0x40011800UL))

#define ADC1_BASEADDRESS                        (((uint32)0x40012400UL))
#define ADC2_BASEADDRESS                        (((uint32)0x40012800UL))
#define TIM1_BASEADDRESS                        (((uint32)0x40012C00UL))
#define SPI1_BASEADDRESS                        (((uint32)0x40013000UL))
#define USART1_BASEADDRESS                      (((uint32)0x40013800UL))
#define DMA_BASEADDRESS                         (((uint32)0x40020000UL))
#define DMA1_BASEADDRESS                        DMA_BASEADDRESS
#define RCC_BASEADDRESS                         (((uint32)0x40021000UL))
#define FLASH_BASEADDRESS                       (((uint32)0x40022000UL))
#define CRC_BASEADDRESS                         (((uint32)0x40023000UL))
                                         
#define SCB_BASEADDRESS                         (((uint32)0xE000ED00UL))
#define NVIC_BASEADDRESS                        (((uint32)0xE000E100UL))
#define STK_BASEADDRESS                         (((uint32)0xE000E010UL))


/*==================================================================================================
*                                              ENUMS
==================================================================================================*/


/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/


/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/


/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/

#ifdef __cplusplus
}
#endif

#endif /* #ifndef BASE_REG_H */

/** @} */   

/** 
*   @file           Spi.h
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/
#ifndef SPI_H
#define SPI_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Std_Types.h"
#include "Spi_Types.h"
#include "Spi_Cfg.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Mcl driver header file and also in the
*        module's description file
*/
#define SPI_MODULE_ID                    1
#define SPI_H_VENDOR_ID                             1
#define SPI_H_AR_RELEASE_MAJOR_VERSION              4
#define SPI_H_AR_RELEASE_MINOR_VERSION              3
#define SPI_H_AR_RELEASE_REVISION_VERSION           1
#define SPI_H_SW_MAJOR_VERSION                      0
#define SPI_H_SW_MINOR_VERSION                      9
#define SPI_H_SW_PATCH_VERSION                      0



/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
///* Check if Spi.h and Std_Types.h file are of the same Autosar version */
//#if ((SPI_H_AR_RELEASE_MAJOR_VERSION    != STD_TYPES_H_AR_RELEASE_MAJOR_VERSION) || \
//     (SPI_H_AR_RELEASE_MINOR_VERSION    != STD_TYPES_H_AR_RELEASE_MINOR_VERSION) || \
//     (SPI_H_AR_RELEASE_REVISION_VERSION != STD_TYPES_H_AR_RELEASE_REVISION_VERSION) \
//    )
//    #error "AutoSar Version Numbers of Spi.h and Std_Types.h are different"
//#endif
///* Check if Spi.h and Std_Types.h header file are of the same Software version */
//#if ((SPI_H_SW_MAJOR_VERSION != STD_TYPES_H_SW_MAJOR_VERSION) || \
//     (SPI_H_SW_MINOR_VERSION != STD_TYPES_H_SW_MINOR_VERSION) || \
//     (SPI_H_SW_PATCH_VERSION != STD_TYPES_H_SW_PATCH_VERSION)    \
//    )
//    #error "Software Version Numbers of Spi.h and Std_Types.h are different"
//#endif



/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/



/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/
#define SPI_INSTANCE_ID                    ((uint8) 083)

/* Error Values */
/**
* @brief API service called with wrong parameter.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_PARAM_CHANNEL       ((uint8)0x0Au)
/**
* @brief API service called with wrong parameter.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_PARAM_JOB           ((uint8)0x0Bu)
/**
* @brief API service called with wrong parameter.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_PARAM_SEQ           ((uint8)0x0Cu)
/**
* @brief API service called with wrong parameter.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_PARAM_LENGTH        ((uint8)0x0Du)
/**
* @brief API service called with wrong parameter.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_PARAM_UNIT          ((uint8)0x0Eu)
/**
* @brief   If the parameter versioninfo is NULL_PTR.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_PARAM_POINTER           ((uint8)0x10u)
/**
* @brief API service used without module initialization.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_UNINIT              ((uint8)0x1Au)
/**
* @brief Services called in a wrong sequence.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_SEQ_PENDING         ((uint8)0x2Au)
/**
* @brief Synchronous transmission service called at wrong time.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_SEQ_IN_PROCESS      ((uint8)0x3Au)
/**
* @brief API SPI_Init service called while the SPI driver has already been initialized.
*
* @implements Spi_ErrorCodes_define
*/
#define SPI_E_ALREADY_INITIALIZED ((uint8)0x4Au)

/* Service IDs */
/**
* @brief API service ID for SPI Init function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_INIT_ID              ((uint8) 0x00u)
/**
* @brief API service ID for SPI DeInit function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_DEINIT_ID            ((uint8) 0x01u)
/**
* @brief API service ID for SPI write IB function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_WRITEIB_ID           ((uint8) 0x02u)
/**
* @brief API service ID for SPI async transmit function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_ASYNCTRANSMIT_ID     ((uint8) 0x03u)
/**
* @brief API service ID for SPI read IB function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_READIB_ID            ((uint8) 0x04u)
/**
* @brief API service ID for SPI setup EB function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_SETUPEB_ID           ((uint8) 0x05u)
/**
* @brief API service ID for SPI get status function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_GETSTATUS_ID         ((uint8) 0x06u)
/**
* @brief API service ID for SPI get job result function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_GETJOBRESULT_ID      ((uint8) 0x07u)
/**
* @brief API service ID for SPI get sequence result function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_GETSEQUENCERESULT_ID ((uint8) 0x08u)
/**
* @brief API service ID for SPI get version info function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_GETVERSIONINFO_ID    ((uint8) 0x09u)
/**
* @brief API service ID for SPI sync transmit function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_SYNCTRANSMIT_ID      ((uint8) 0x0Au)
/**
* @brief API service ID for SPI get hwunit status function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_GETHWUNITSTATUS_ID   ((uint8) 0x0Bu)
/**
* @brief API service ID for SPI cancel function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_CANCEL_ID            ((uint8) 0x0Cu)
/**
* @brief API service ID for SPI set async mode function.
* @details Parameters used when raising an error or exception.
*
* @implements Spi_ServiceIds_Define
*/
#define SPI_SERVICE_SETASYNCMODE_ID      ((uint8) 0x0Du)



/*==================================================================================================
*                                              ENUMS
==================================================================================================*/



/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/



/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/


/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/
/* Allocate defined section for SPI code */
#define SPI_START_SEC_CODE
#include "Spi_MemMap.h"

FUNC(void, SPI_CODE) Spi_Init(P2CONST(Spi_ConfigType, AUTOMATIC, SPI_APPL_CONST) ConfigPtr);
FUNC(Std_ReturnType, SPI_CODE) Spi_DeInit(void);
FUNC (Std_ReturnType, SPI_CODE) Spi_WriteIB(
        VAR(Spi_ChannelType, AUTOMATIC) Channel,
        P2CONST(Spi_DataBufferType, AUTOMATIC, SPI_APPL_CONST) DataBufferPtr
    );
FUNC (Std_ReturnType, SPI_CODE) Spi_ReadIB(
        VAR(Spi_ChannelType, AUTOMATIC) Channel,
        P2VAR(Spi_DataBufferType, AUTOMATIC, SPI_APPL_DATA) DataBufferPtr
    );

FUNC (Std_ReturnType, SPI_CODE) Spi_SetupEB(
        VAR(Spi_ChannelType, AUTOMATIC) Channel,
        P2CONST(Spi_DataBufferType, AUTOMATIC, SPI_APPL_DATA) SrcDataBufferPtr,
        P2VAR(Spi_DataBufferType, AUTOMATIC, SPI_APPL_DATA) DestDataBufferPtr,
        VAR(Spi_NumberOfDataType, AUTOMATIC) Length
    );

FUNC (Std_ReturnType, SPI_CODE) Spi_SyncTransmit(VAR(Spi_SequenceType, AUTOMATIC) Sequence);
FUNC (Std_ReturnType, SPI_CODE) Spi_AsyncTransmit(VAR(Spi_SequenceType, AUTOMATIC) Sequence);
FUNC (Spi_StatusType, SPI_CODE) Spi_GetStatus(void);
FUNC (Spi_JobResultType, SPI_CODE) Spi_GetJobResult(VAR(Spi_JobType, AUTOMATIC) Job);    
FUNC (Spi_StatusType, SPI_CODE) Spi_GetHWUnitStatus(VAR(Spi_HWUnitType, AUTOMATIC) HWUnit);
FUNC (void, SPI_CODE) Spi_Cancel(VAR(Spi_SequenceType, AUTOMATIC) Sequence);
FUNC (Std_ReturnType, SPI_CODE) Spi_SetAsyncMode(VAR(Spi_AsyncModeType, AUTOMATIC) AsyncMode);
FUNC (void, SPI_CODE) Spi_GetVersionInfo(
    P2VAR(Std_VersionInfoType, AUTOMATIC, SPI_APPL_DATA) versioninfo
);
FUNC (Spi_SeqResultType, SPI_CODE) Spi_GetSequenceResult
(
    VAR(Spi_SequenceType, AUTOMATIC) Sequence
);
FUNC (void, SPI_CODE) Spi_MainFunction_Handling(void);
#define SPI_STOP_SEC_CODE
#include "Spi_MemMap.h"

#ifdef __cplusplus
}
#endif

#endif /* #ifndef SPI_H */

/** @} */

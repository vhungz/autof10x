 /** 
*   @file           Spi_Types.h
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/
#ifndef SPI_TYPES_H
#define SPI_TYPES_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Std_Types.h"
#include "Base_REG.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Spi driver header file and also in the
*        module's description file
*/
#define SPI_TYPES_H_VENDOR_ID                             1
#define SPI_TYPES_H_AR_RELEASE_MAJOR_VERSION              4
#define SPI_TYPES_H_AR_RELEASE_MINOR_VERSION              3
#define SPI_TYPES_H_AR_RELEASE_REVISION_VERSION           1
#define SPI_TYPES_H_SW_MAJOR_VERSION                      0
#define SPI_TYPES_H_SW_MINOR_VERSION                      9
#define SPI_TYPES_H_SW_PATCH_VERSION                      0


/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/


/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/
#define SPI_HW_CFG_CS_POLARITY                  BIT_0
#define SPI_HW_CFG_CS_SELECTION                 BIT_1
#define SPI_HW_CFG_CS_ENABLE                    BIT_2
#define SPI_HW_CFG_DATA_SHIFT_EDGE              BIT_3
#define SPI_HW_CFG_SHIFT_CLOCK_IDLE_LEVEL       BIT_4

#define SPI_CHANNEL_CFG_EB_TYPE                 BIT_0

#define SPI_PHYUNIT_DMA                         BIT_0



/*==================================================================================================
*                                              ENUMS
==================================================================================================*/
/**
* @brief   This type defines a range of specific status for SPI Driver.
*
* @implements Spi_StatusType_enumeration
*/
typedef enum
{
    SPI_UNINIT = 0,          /**< @brief Not initialized or not usable. */
    SPI_IDLE,                /**< @brief Not currently transmitting any jobs. */
    SPI_BUSY                 /**< @brief Is performing a SPI Job(transmit). */
} Spi_StatusType;

/**
* @brief   This type defines a range of specific Jobs status for SPI Driver.
*
* @implements Spi_JobResultType_enumeration
*/
typedef enum
{
    SPI_JOB_OK = 0,     /**< @brief The last transmission of the Job has been finished successfully. */
    SPI_JOB_PENDING,    /**< @brief The SPI handler/Driver is performing a SPI Job. */
    SPI_JOB_FAILED,     /**< @brief The last transmission of the Job has failed. */
    SPI_JOB_QUEUED      /**< @brief An asynchronous transmit Job has been accepted, while actual
                                     transmission for this Job has not started yet. */
} Spi_JobResultType;

/**
* @brief   This type defines a range of specific Sequences status for SPI Driver.
*
* @implements Spi_SeqResultType_enumeration
*/
typedef enum
{
    SPI_SEQ_OK = 0, /**< @brief The last transmission of the Sequence has been finished successfully. */
    SPI_SEQ_PENDING,         /**< @brief The SPI handler/Driver is performing a SPI Sequence. */
    SPI_SEQ_FAILED,          /**< @brief The last transmission of the Sequence has failed. */
    SPI_SEQ_CANCELLED   /**< @brief The last transmission of the Sequence has been cancelled by the user. */
} Spi_SeqResultType;


/**
* @brief   Specifies the asynchronous mechanism mode for SPI buses handled asynchronously in Level 2
* @details #if (LEVEL2 == SPI_LEVEL_DELIVERED)
*            Specifies the asynchronous mechanism mode for SPI buses handled
*            asynchronously in LEVEL 2. SPI150: This type is available or not
*           according to the pre compile time parameter:
*            SPI_LEVEL_DELIVERED. This is only relevant for LEVEL 2.
*
* @implements Spi_AsyncModeType_enumeration
*/
typedef enum
{
    /* The asynchronous mechanism is ensured by polling, so interrupts
       related to SPI buses handled asynchronously are disabled. */
    SPI_POLLING_MODE = 0,
    /* The asynchronous mechanism is ensured by interrupt, so interrupts
       related to SPI buses handled asynchronously are enabled. */
    SPI_INTERRUPT_MODE
} Spi_AsyncModeType;





/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/
/**
* @brief   Type of application data buffer elements.
*
* @implements Spi_DataBufferType_typedef
*/
typedef uint8 Spi_DataBufferType;

/**
* @brief   Type for defining the number of data elements of the type Spi_DataType.
* @details Type for defining the number of data elements of the type Spi_DataType
*          to send or receive by Channel.
*
* @implements Spi_NumberOfDataType_typedef
*/
typedef uint16 Spi_NumberOfDataType;

/**
* @brief   Specifies the identification (ID) for a Channel.
*
* @implements Spi_ChannelType_typedef
*/
typedef uint32 Spi_ChannelType;

/**
* @brief   Specifies the identification (ID) for a Job.
*
* @implements Spi_JobType_typedef
*/
typedef uint32 Spi_JobType;

/**
* @brief   Specifies the identification (ID) for a sequence of jobs.
*
* @implements Spi_SequenceType_typedef
*/
typedef uint32 Spi_SequenceType;

/**
* @brief     Specifies the ID for a SPI Hardware microcontroller peripheral unit.
* @details  This type is used for specifying the identification (ID) for a SPI
*            Hardware microcontroller peripheral unit.
*
* @implements Spi_HWUnitType_typedef
*/
typedef uint8 Spi_HWUnitType;

typedef void (*Spi_SeqNotifyType)(void);
typedef void (*Spi_JobNotifyType)(void);


typedef struct
{
    P2VAR(Spi_DataBufferType, SPI_VAR, SPI_APPL_DATA) pBufferTX;
    P2VAR(Spi_DataBufferType, SPI_VAR, SPI_APPL_DATA) pBufferRX;
} Spi_BufferDescriptorType;


typedef struct
{
    VAR(uint32, SPI_VAR) u32HwBaseAdddress; 
    VAR(uint8, SPI_VAR) Config;
    VAR(Spi_StatusType, SPI_VAR) Status;
    P2VAR(uint32, SPI_VAR, SPI_APPL_CONST) pCurrJob;
    VAR(uint16, AUTOMATIC) u16DMATxChannel;
    VAR(uint16, AUTOMATIC) u16DMARxChannel;
} Spi_PhyHwUnitType;

typedef struct
{
    /*
    Bit 0: CS Polarity
    Bit 1: CS Selection
    Bit 2: CS Enable
    Bit 3: DataShiftEdge
    Bit 4: ShiftClockIdleLevel
    */
    VAR(uint8, SPI_VAR) u8Config;
    VAR(uint32, SPI_VAR) u32Baudrate;
    CONST(uint16, AUTOMATIC) CsChannelId;
    VAR(uint8, SPI_VAR) u8CsIdentifier;
    P2VAR(Spi_PhyHwUnitType, SPI_VAR, SPI_APPL_DATA) HwUnit; 
    VAR(uint32, SPI_VAR) u32TimeClk2Cs;
} Spi_ExternalDeviceConfigType;

/**
* @brief   The structure contains the channel configuration parameters.
*
* @implements Spi_ChannelConfigType_struct
*/
typedef struct
{
    /**< @brief Buffer Type IB/EB. */
    /*
    Bit 0: Buffer type. 0 IB, 1 EB
    Bit 1: Transfer start . 0 MSB, 1 LSB
    */
    VAR(uint8, SPI_VAR) u8Config;
    VAR(uint8, SPI_VAR) u8DataWidth;
    VAR(uint32, SPI_VAR) u32DefaultData;
    /*
    VAR(uint16, SPI_VAR) u16EbMaxLength;
    VAR(uint16, SPI_VAR) u16IbNBuffers;
    */
    VAR(uint16, SPI_VAR) u16CurrIndex;
    VAR(uint16, SPI_VAR) u16RecvIndex;
    VAR(uint16, SPI_VAR) u16BuffLength;
    VAR(uint16, SPI_VAR) u16MaxBuffLength;
    P2VAR(Spi_BufferDescriptorType, SPI_VAR, SPI_APPL_DATA) pcBufferDescriptor;
} Spi_ChannelConfigType;

/**
* @brief   This is the structure containing all the parameters needed to completely define a Job.
*
* @implements Spi_JobConfigType_struct
*/
typedef struct
{
    /**< @brief Number of channels in the job. */
    VAR(Spi_ChannelType, SPI_VAR) NumChannels;
    /*
    Bit 0: Sync mode
    */
    VAR(uint8, SPI_VAR) u8Config;
    VAR(uint8, SPI_VAR) u8Priority;
    VAR(uint8, SPI_VAR) u8CurrChannel;
    VAR(Spi_JobResultType, SPI_VAR) Status;
    P2VAR(Spi_ExternalDeviceConfigType, SPI_VAR, SPI_APPL_CONST) pDevice;
    /**< @brief Channel index list. */
    P2VAR(Spi_ChannelConfigType*, SPI_VAR, SPI_APPL_CONST) pcChannelList;
    P2VAR(uint32, SPI_VAR, SPI_APPL_CONST) pNextJob;
    VAR(Spi_JobNotifyType, Spi_VAR) EndJobNotification;
} Spi_JobConfigType;

/**
* @brief   This structure contains all the needed data to configure one SPI Sequence.
*
* @implements Spi_SequenceConfigType_struct
*/
typedef struct
{
    VAR(Spi_SeqResultType, SPI_VAR) Status;
    /**< @brief Number of jobs in the sequence. */
    VAR(Spi_JobType, SPI_VAR) NumJobs;
    /**< @brief Job index list. */
    P2VAR(Spi_JobConfigType*, SPI_VAR, SPI_APPL_CONST) pcJobList;
    VAR(uint16, SPI_VAR) u16RemainingJobs;
    VAR(Spi_SeqNotifyType, Spi_VAR) EndSeqNotification;
} Spi_SequenceConfigType;

/**
* @brief   This is the top level structure containing all the 
*          needed parameters for the SPI Handler Driver.
*
* @implements Spi_ConfigType_struct
*/
typedef struct
{
    /**< @brief Number of external devices defined in the configuration. */
    VAR(uint16, SPI_VAR) u16MaxExternalDevice;
    /**< @brief Number of channels defined in the configuration. */
    VAR(uint8, SPI_VAR) u8SpiMaxChannel;
    /**< @brief Number of jobs defined in the configuration. */
    VAR(uint8, SPI_VAR) u8SpiMaxJob;
    /**< @brief Number of sequences defined in the configuration. */
    VAR(uint8, SPI_VAR) u8SpiMaxSequence;
    /**< @brief Pointer to Array of channels defined in the configuration. */
    CONSTP2CONST(Spi_ChannelConfigType, SPI_VAR, SPI_APPL_CONST) pcChannelConfig;
    /**< @brief Pointer to Array of jobs defined in the configuration. */
    CONSTP2CONST(Spi_JobConfigType, SPI_VAR, SPI_APPL_CONST) pcJobConfig;
    /**< @brief Pointer to Array of sequences defined in the configuration. */
    CONSTP2CONST(Spi_SequenceConfigType, SPI_VAR, SPI_APPL_CONST) pcSequenceConfig;
    
    CONSTP2CONST(Spi_ExternalDeviceConfigType, SPI_VAR, SPI_APPL_CONST) pcDeviceConfig;
    
} Spi_ConfigType;


/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/


/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/


#ifdef __cplusplus
}
#endif

#endif /* #ifndef SPI_TYPES_H */

/** @} */   

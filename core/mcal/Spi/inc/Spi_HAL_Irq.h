/** 
*   @file           Spi_HAL_Irq.h
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/
#ifndef SPI_HAL_IRQ_H
#define SPI_HAL_IRQ_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Std_Types.h"
#include "Spi_Cfg.h"
#include "Spi_HAL.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Mcl driver header file and also in the
*        module's description file
*/
#define SPI_HAL_IRQ_H_VENDOR_ID                             1
#define SPI_HAL_IRQ_H_AR_RELEASE_MAJOR_VERSION              4
#define SPI_HAL_IRQ_H_AR_RELEASE_MINOR_VERSION              3
#define SPI_HAL_IRQ_H_AR_RELEASE_REVISION_VERSION           1
#define SPI_HAL_IRQ_H_SW_MAJOR_VERSION                      0
#define SPI_HAL_IRQ_H_SW_MINOR_VERSION                      9
#define SPI_HAL_IRQ_H_SW_PATCH_VERSION                      0



/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/



/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/



/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/


/*==================================================================================================
*                                              ENUMS
==================================================================================================*/


/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/


/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/
extern FUNC(void, SPI_CODE) SPI1_IRQHandler(VAR(void, AUTOMATIC));
extern FUNC(void, SPI_CODE) SPI2_IRQHandler(VAR(void, AUTOMATIC));

/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/
/* Allocate defined section for SPI code */
#define SPI_START_SEC_CODE
#include "Spi_MemMap.h"



#define SPI_STOP_SEC_CODE
#include "Spi_MemMap.h"

#ifdef __cplusplus
}
#endif

#endif /* #ifndef SPI_HAL_IRQ_H */

/** @} */

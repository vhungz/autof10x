 /** 
*   @file           Spi_REG.h
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/
#ifndef SPI_REG_H
#define SPI_REG_H

#ifdef __cplusplus
extern "C"{
#endif

/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Std_Types.h"
#include "Base_REG.h"
#include "Spi_Cfg.h"

/*==================================================================================================
*                               SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Mcl driver header file and also in the
*        module's description file
*/
#define SPI_REG_H_VENDOR_ID                             1
#define SPI_REG_H_AR_RELEASE_MAJOR_VERSION              4
#define SPI_REG_H_AR_RELEASE_MINOR_VERSION              3
#define SPI_REG_H_AR_RELEASE_REVISION_VERSION           1
#define SPI_REG_H_SW_MAJOR_VERSION                      0
#define SPI_REG_H_SW_MINOR_VERSION                      9
#define SPI_REG_H_SW_PATCH_VERSION                      0



/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
///* Check if Spi_REG.h and Std_Types.h file are of the same Autosar version */
//#if ((SPI_REG_H_AR_RELEASE_MAJOR_VERSION    != STD_TYPES_H_AR_RELEASE_MAJOR_VERSION) || \
//     (SPI_REG_H_AR_RELEASE_MINOR_VERSION    != STD_TYPES_H_AR_RELEASE_MINOR_VERSION) || \
//     (SPI_REG_H_AR_RELEASE_REVISION_VERSION != STD_TYPES_H_AR_RELEASE_REVISION_VERSION) \
//    )
//    #error "AutoSar Version Numbers of Spi_REG.h and Std_Types.h are different"
//#endif
///* Check if Spi_REG.h and Std_Types.h header file are of the same Software version */
//#if ((SPI_REG_H_SW_MAJOR_VERSION != STD_TYPES_H_SW_MAJOR_VERSION) || \
//     (SPI_REG_H_SW_MINOR_VERSION != STD_TYPES_H_SW_MINOR_VERSION) || \
//     (SPI_REG_H_SW_PATCH_VERSION != STD_TYPES_H_SW_PATCH_VERSION)    \
//    )
//    #error "Software Version Numbers of Spi_REG.h and Std_Types.h are different"
//#endif

/* Check if Spi_REG.h and Base_REG.h file are of the same Autosar version */
#if ((SPI_REG_H_AR_RELEASE_MAJOR_VERSION    != BASE_REG_H_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_REG_H_AR_RELEASE_MINOR_VERSION    != BASE_REG_H_AR_RELEASE_MINOR_VERSION) || \
     (SPI_REG_H_AR_RELEASE_REVISION_VERSION != BASE_REG_H_AR_RELEASE_REVISION_VERSION) \
    )
    #error "AutoSar Version Numbers of Spi_REG.h and Base_REG.h are different"
#endif
/* Check if Spi_REG.h and Base_REG.h header file are of the same Software version */
#if ((SPI_REG_H_SW_MAJOR_VERSION != BASE_REG_H_SW_MAJOR_VERSION) || \
     (SPI_REG_H_SW_MINOR_VERSION != BASE_REG_H_SW_MINOR_VERSION) || \
     (SPI_REG_H_SW_PATCH_VERSION != BASE_REG_H_SW_PATCH_VERSION)    \
    )
    #error "Software Version Numbers of Spi_REG.h and Base_REG.h are different"
#endif




/*==================================================================================================
*                                          CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      DEFINES AND MACROS
==================================================================================================*/

#define SPI_NSS     1

#define SPI_CR1_SSM_SSI_MASK    (BIT_8 | BIT_9)
#define SPI_CR1_SSM             BIT_9
#define SPI_CR1_SSI             BIT_8
#define SPI_CR1_SPE             BIT_6
#define SPI_CR1_BR_MASK         (BIT_3 | BIT_4 | BIT_5)
#define SPI_CR1_BR_INDEX        BIT_3_INDEX
#define SPI_CR1_MSTR            BIT_2
#define SPI_CR1_CPOL            BIT_1
#define SPI_CR1_CPOL_INDEX      BIT_1_INDEX
#define SPI_CR1_CPHA            BIT_0
#define SPI_CR1_CPHA_INDEX      BIT_0_INDEX

#define SPI_CR2_TXEIE           BIT_7
#define SPI_CR2_RXEIE           BIT_6
#define SPI_CR2_SSOE            BIT_2
#define SPI_CR2_TXDMAEN         BIT_1
#define SPI_CR2_RXDMAEN         BIT_0

#define SPI_SR_BSY              BIT_7
#define SPI_SR_TXE              BIT_1
#define SPI_SR_RXE              BIT_0

#define SPI_CR1_ADDRESS32(SPI_BASEADDRES)     (SPI_BASEADDRES)
#define SPI_CR2_ADDRESS32(SPI_BASEADDRES)     (SPI_BASEADDRES + 0x04)
#define SPI_SR_ADDRESS32(SPI_BASEADDRES)      (SPI_BASEADDRES + 0x08)
#define SPI_DR_ADDRESS32(SPI_BASEADDRES)      (SPI_BASEADDRES + 0x0C)
#define SPI_CRCPR_ADDRESS32(SPI_BASEADDRES)   (SPI_BASEADDRES + 0x10)
#define SPI_RXCRCR_ADDRESS32(SPI_BASEADDRES)  (SPI_BASEADDRES + 0x14)
#define SPI_TXCRCR_ADDRESS32(SPI_BASEADDRES)  (SPI_BASEADDRES + 0x18)
#define SPI_I2SCFGR_ADDRESS32(SPI_BASEADDRES) (SPI_BASEADDRES + 0x1C)
#define SPI_I2SPR_ADDRESS32(SPI_BASEADDRES)   (SPI_BASEADDRES + 0x20)


/*==================================================================================================
*                                              ENUMS
==================================================================================================*/


/*==================================================================================================
*                                STRUCTURES AND OTHER TYPEDEFS
==================================================================================================*/


/*==================================================================================================
*                                GLOBAL VARIABLE DECLARATIONS
==================================================================================================*/


/*==================================================================================================
*                                     FUNCTION PROTOTYPES
==================================================================================================*/
/* Allocate defined section for SPI code */
#define SPI_START_SEC_CODE
//#include "Spi_MemMap.h"

FUNC (void, SPI_CODE) Spi_REG_HWInit
    (
        P2VAR(Spi_PhyHwUnitType, AUTOMATIC, SPI_APPL_CONST) HwUnit
    );

FUNC (void, SPI_CODE) Spi_REG_SyncTransmit(
    P2CONST(Spi_ExternalDeviceConfigType, AUTOMATIC, SPI_APPL_CONST) HwConfig, 
    P2CONST(Spi_ChannelConfigType, AUTOMATIC, SPI_APPL_CONST) Channel
    );
FUNC (void, SPI_CODE) Spi_REG_AsyncTransmit(
    P2CONST(Spi_JobConfigType, SPI_VAR, SPI_APPL_CONST) pJob
    );
FUNC (void, SPI_CODE) Spi_REG_EnableDMATransmit(P2CONST(Spi_JobConfigType, SPI_VAR, SPI_APPL_CONST) pJob);
FUNC (void, SPI_CODE) Spi_REG_DisableDMATransmit(P2CONST(Spi_JobConfigType, SPI_VAR, SPI_APPL_CONST) pJob);
FUNC (void, SPI_CODE) Spi_REG_CompleteTransmit(
    P2CONST(Spi_ExternalDeviceConfigType, AUTOMATIC, SPI_APPL_CONST) HwConfig
    );
#define SPI_STOP_SEC_CODE
//#include "Spi_MemMap.h"

#ifdef __cplusplus
}
#endif

#endif /* #ifndef SPI_H */

/** @} */  

 /** 
*   @file           Spi_REG.c
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/

#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Spi_REG.h"
#include "SchM_Spi.h"
#include "StdRegMacros.h"

#ifdef SPI_ENABLE_CS_GPIO
#if (SPI_ENABLE_CS_GPIO == STD_ON)
#include "Dio.h"
#endif
#endif

/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Mcl driver header file and also in the
*        module's description file
*/
#define SPI_REG_C_VENDOR_ID                             1
#define SPI_REG_C_AR_RELEASE_MAJOR_VERSION              4
#define SPI_REG_C_AR_RELEASE_MINOR_VERSION              3
#define SPI_REG_C_AR_RELEASE_REVISION_VERSION           1
#define SPI_REG_C_SW_MAJOR_VERSION                      0
#define SPI_REG_C_SW_MINOR_VERSION                      9
#define SPI_REG_C_SW_PATCH_VERSION                      0



/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
/* Check if Spi_REG.c and Spi_REG.h file are of the same Autosar version */
#if ((SPI_REG_C_AR_RELEASE_MAJOR_VERSION    != SPI_REG_H_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_REG_C_AR_RELEASE_MINOR_VERSION    != SPI_REG_H_AR_RELEASE_MINOR_VERSION) || \
     (SPI_REG_C_AR_RELEASE_REVISION_VERSION != SPI_REG_H_AR_RELEASE_REVISION_VERSION) \
    )
    #error "AutoSar Version Numbers of Spi_REG.c and Spi_REG.h are different"
#endif
/* Check if Spi_REG.c and Spi_REG.h header file are of the same Software version */
#if ((SPI_REG_C_SW_MAJOR_VERSION != SPI_REG_H_SW_MAJOR_VERSION) || \
     (SPI_REG_C_SW_MINOR_VERSION != SPI_REG_H_SW_MINOR_VERSION) || \
     (SPI_REG_C_SW_PATCH_VERSION != SPI_REG_H_SW_PATCH_VERSION)    \
    )
    #error "Software Version Numbers of Spi_REG.c and Spi_REG.h are different"
#endif

/* Check if Spi_REG.c and StdRegMacros.h file are of the same Autosar version */
#if ((SPI_REG_C_AR_RELEASE_MAJOR_VERSION    != STDREGMACROS_H_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_REG_C_AR_RELEASE_MINOR_VERSION    != STDREGMACROS_H_AR_RELEASE_MINOR_VERSION) || \
     (SPI_REG_C_AR_RELEASE_REVISION_VERSION != STDREGMACROS_H_AR_RELEASE_REVISION_VERSION) \
    )
    #error "AutoSar Version Numbers of Spi_REG.c and StdRegMacros.h are different"
#endif
/* Check if Spi_REG.c and StdRegMacros.h header file are of the same Software version */
#if ((SPI_REG_C_SW_MAJOR_VERSION != STDREGMACROS_H_SW_MAJOR_VERSION) || \
     (SPI_REG_C_SW_MINOR_VERSION != STDREGMACROS_H_SW_MINOR_VERSION) || \
     (SPI_REG_C_SW_PATCH_VERSION != STDREGMACROS_H_SW_PATCH_VERSION)    \
    )
    #error "Software Version Numbers of Spi_REG.c and StdRegMacros.h are different"
#endif





/*==================================================================================================
*                         LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/
#define SPI_START_SEC_VAR_INIT_UNSPECIFIED
#include "Spi_MemMap.h"


#define SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "Spi_MemMap.h"

/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/
FUNC (void, SPI_CODE) Spi_REG_PrepareHW(
    P2CONST(Spi_ExternalDeviceConfigType, AUTOMATIC, SPI_APPL_CONST) HwConfig
);

/*==================================================================================================
*                                      LOCAL FUNCTIONS
==================================================================================================*/
#define SPI_START_SEC_CODE
#include "Spi_MemMap.h"

FUNC (void, SPI_CODE) Spi_REG_PrepareHW(
    P2CONST(Spi_ExternalDeviceConfigType, AUTOMATIC, SPI_APPL_CONST) HwConfig
)
{
    uint16 u16Data = REG_READ16(SPI_CR1_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress));
    
    RMW16(u16Data, 
            SPI_CR1_BR_MASK | SPI_CR1_CPOL | SPI_CR1_CPHA, 
            (HwConfig->u32Baudrate << SPI_CR1_BR_INDEX)
            | (((HwConfig->u8Config & SPI_HW_CFG_DATA_SHIFT_EDGE) == SPI_HW_CFG_DATA_SHIFT_EDGE ? \
                1 : 0) << SPI_CR1_CPHA_INDEX)
            | (((HwConfig->u8Config & SPI_HW_CFG_SHIFT_CLOCK_IDLE_LEVEL) == \
                SPI_HW_CFG_SHIFT_CLOCK_IDLE_LEVEL ? 1 : 0) << SPI_CR1_CPOL_INDEX)
            );
            
    REG_WRITE16(SPI_CR1_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress), u16Data);

    if ((HwConfig->u8Config & SPI_HW_CFG_CS_ENABLE) == SPI_HW_CFG_CS_ENABLE)
    {
        if ((HwConfig->u8Config & SPI_HW_CFG_CS_SELECTION) == SPI_HW_CFG_CS_SELECTION)
        {
            REG_BIT_SET16(SPI_CR2_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress), SPI_CR2_SSOE);
        }
    }


}

/*==================================================================================================
*                                      GLOBAL FUNCTIONS
==================================================================================================*/

FUNC (void, SPI_CODE) Spi_REG_HWInit
    (
        P2VAR(Spi_PhyHwUnitType, AUTOMATIC, SPI_APPL_CONST) HwUnit
    )
{


	HwUnit->Status = SPI_IDLE;


}

FUNC (void, SPI_CODE) Spi_REG_SyncTransmit(
    P2CONST(Spi_ExternalDeviceConfigType, AUTOMATIC, SPI_APPL_CONST) HwConfig, 
    P2CONST(Spi_ChannelConfigType, AUTOMATIC, SPI_APPL_CONST) Channel
    )
{
    VAR(uint16, SPI_VAR) u16Length;
    P2VAR(Spi_DataBufferType, SPI_VAR, SPI_APPL_DATA) pTXBuffer = NULL_PTR;
    P2VAR(Spi_DataBufferType, SPI_VAR, SPI_APPL_DATA) pRXBuffer = NULL_PTR;
    
    u16Length = Channel->u16BuffLength;
    pTXBuffer = Channel->pcBufferDescriptor->pBufferTX;
    pRXBuffer = Channel->pcBufferDescriptor->pBufferRX;
    
    Spi_REG_PrepareHW(HwConfig);
    REG_RMW16(SPI_CR1_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress), 
        SPI_CR1_MSTR | SPI_CR1_SPE, 
        SPI_CR1_MSTR | SPI_CR1_SPE);

    while (u16Length > 0)
    {
        while (!BIT_IS_SET16(REG_READ16(SPI_SR_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress)),
            SPI_SR_TXE));
        REG_WRITE16(SPI_DR_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress), *pTXBuffer);
        while (!BIT_IS_SET16(REG_READ16(SPI_SR_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress)),
            SPI_SR_RXE));
        if (pRXBuffer != NULL_PTR)
        {
            *pRXBuffer = REG_READ16(SPI_DR_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress));
            pRXBuffer++;
        }
        else
        {
        	REG_READ16(SPI_DR_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress));
        }
        pTXBuffer++;
        u16Length--;
    }
    REG_BIT_CLEAR16(SPI_CR1_ADDRESS32(HwConfig->HwUnit->u32HwBaseAdddress), 
        SPI_CR1_MSTR | SPI_CR1_SPE);
}




#define SPI_STOP_SEC_CODE
#include "Spi_MemMap.h"

#ifdef __cplusplus
}
#endif


/** @} */

/** 
*   @file           Spi_HAL.c
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/

#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Spi_HAL.h"
#include "SchM_Spi.h"
#include "StdRegMacros.h"

/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Mcl driver header file and also in the
*        module's description file
*/
#define SPI_HAL_C_VENDOR_ID                             1
#define SPI_HAL_C_AR_RELEASE_MAJOR_VERSION              4
#define SPI_HAL_C_AR_RELEASE_MINOR_VERSION              3
#define SPI_HAL_C_AR_RELEASE_REVISION_VERSION           1
#define SPI_HAL_C_SW_MAJOR_VERSION                      0
#define SPI_HAL_C_SW_MINOR_VERSION                      9
#define SPI_HAL_C_SW_PATCH_VERSION                      0



/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
///* Check if Spi_HAL.c and Spi_HAL.h file are of the same Autosar version */
//#if ((SPI_HAL_C_AR_RELEASE_MAJOR_VERSION    != SPI_HAL_H_AR_RELEASE_MAJOR_VERSION) || \
//     (SPI_HAL_C_AR_RELEASE_MINOR_VERSION    != SPI_HAL_H_AR_RELEASE_MINOR_VERSION) || \
//     (SPI_HAL_C_AR_RELEASE_REVISION_VERSION != SPI_HAL_H_AR_RELEASE_REVISION_VERSION) \
//    )
//    #error "AutoSar Version Numbers of Spi_HAL.c and Spi_HAL.h are different"
//#endif
///* Check if Spi_HAL.c and Spi_HAL.h header file are of the same Software version */
//#if ((SPI_HAL_C_SW_MAJOR_VERSION != SPI_HAL_H_SW_MAJOR_VERSION) || \
//     (SPI_HAL_C_SW_MINOR_VERSION != SPI_HAL_H_SW_MINOR_VERSION) || \
//     (SPI_HAL_C_SW_PATCH_VERSION != SPI_HAL_H_SW_PATCH_VERSION)    \
//    )
//    #error "Software Version Numbers of Spi_HAL.c and Spi_HAL.h are different"
//#endif
//
///* Check if Spi_HAL.c and Spi_REG.h file are of the same Autosar version */
//#if ((SPI_HAL_C_AR_RELEASE_MAJOR_VERSION    != SPI_REG_H_AR_RELEASE_MAJOR_VERSION) || \
//     (SPI_HAL_C_AR_RELEASE_MINOR_VERSION    != SPI_REG_H_AR_RELEASE_MINOR_VERSION) || \
//     (SPI_HAL_C_AR_RELEASE_REVISION_VERSION != SPI_REG_H_AR_RELEASE_REVISION_VERSION) \
//    )
//    #error "AutoSar Version Numbers of Spi_HAL.c and Spi_REG.h are different"
//#endif
///* Check if Spi_HAL.c and Spi_REG.h header file are of the same Software version */
//#if ((SPI_HAL_C_SW_MAJOR_VERSION != SPI_REG_H_SW_MAJOR_VERSION) || \
//     (SPI_HAL_C_SW_MINOR_VERSION != SPI_REG_H_SW_MINOR_VERSION) || \
//     (SPI_HAL_C_SW_PATCH_VERSION != SPI_REG_H_SW_PATCH_VERSION)    \
//    )
//    #error "Software Version Numbers of Spi_HAL.c and Spi_REG.h are different"
//#endif




/*==================================================================================================
*                         LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/
#define SPI_START_SEC_VAR_INIT_UNSPECIFIED
#include "Spi_MemMap.h"


#define SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "Spi_MemMap.h"

/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL FUNCTIONS
==================================================================================================*/
#define SPI_START_SEC_CODE
#include "Spi_MemMap.h"


/*==================================================================================================
*                                      GLOBAL FUNCTIONS
==================================================================================================*/



#define SPI_STOP_SEC_CODE
#include "Spi_MemMap.h"

#ifdef __cplusplus
}
#endif


/** @} */

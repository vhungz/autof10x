/** 
*   @file           Spi.c
*   @version        0.9.0
*   @brief          AUTOSAR Spi - <TODO>
*
*   @details        <TODO>
*   (c) Copyright 2017-2019 Mina HiTech Ltd 
*   All rights reserved.
*
*   @addtogroup SPI_MODULE
*   @{
*/ 
/*==================================================================================================
==================================================================================================*/
/**
* @page misra_violations MISRA-C:2004 violations
*
*/

#ifdef __cplusplus
extern "C"{
#endif


/*==================================================================================================
*                                        INCLUDE FILES
==================================================================================================*/
#include "Spi.h"
#include "SchM_Spi.h"
#if (SPI_DEV_ERROR_DETECT == STD_ON)
#include "Det.h"
#endif /*(SPI_DEV_ERROR_DETECT == STD_ON)*/
#include "Spi_HAL.h"

/*==================================================================================================
*                              SOURCE FILE VERSION INFORMATION
==================================================================================================*/
/**
* @brief Parameters that shall be published within the Mcl driver header file and also in the
*        module's description file
*/
#define SPI_C_VENDOR_ID                             1
#define SPI_C_AR_RELEASE_MAJOR_VERSION              4
#define SPI_C_AR_RELEASE_MINOR_VERSION              3
#define SPI_C_AR_RELEASE_REVISION_VERSION           1
#define SPI_C_SW_MAJOR_VERSION                      0
#define SPI_C_SW_MINOR_VERSION                      9
#define SPI_C_SW_PATCH_VERSION                      0



/*==================================================================================================
*                                     FILE VERSION CHECKS
==================================================================================================*/
/* Check if Spi.c and Spi.h file are of the same Autosar version */
#if ((SPI_C_AR_RELEASE_MAJOR_VERSION    != SPI_H_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_C_AR_RELEASE_MINOR_VERSION    != SPI_H_AR_RELEASE_MINOR_VERSION) || \
     (SPI_C_AR_RELEASE_REVISION_VERSION != SPI_H_AR_RELEASE_REVISION_VERSION) \
    )
    #error "AutoSar Version Numbers of Spi.c and Spi.h are different"
#endif
/* Check if Spi.c and Spi.h header file are of the same Software version */
#if ((SPI_C_SW_MAJOR_VERSION != SPI_H_SW_MAJOR_VERSION) || \
     (SPI_C_SW_MINOR_VERSION != SPI_H_SW_MINOR_VERSION) || \
     (SPI_C_SW_PATCH_VERSION != SPI_H_SW_PATCH_VERSION)    \
    )
    #error "Software Version Numbers of Spi.c and Spi.h are different"
#endif

/* Check if Spi.c and SchM_Spi.h file are of the same Autosar version */
//#if ((SPI_C_AR_RELEASE_MAJOR_VERSION    != SCHM_SPI_H_AR_RELEASE_MAJOR_VERSION) || \
//     (SPI_C_AR_RELEASE_MINOR_VERSION    != SCHM_SPI_H_AR_RELEASE_MINOR_VERSION) || \
//     (SPI_C_AR_RELEASE_REVISION_VERSION != SCHM_SPI_H_AR_RELEASE_REVISION_VERSION) \
//    )
//    #error "AutoSar Version Numbers of Spi.c and SchM_Spi.h are different"
//#endif
///* Check if Spi.c and SchM_Spi.h header file are of the same Software version */
//#if ((SPI_C_SW_MAJOR_VERSION != SCHM_SPI_H_SW_MAJOR_VERSION) || \
//     (SPI_C_SW_MINOR_VERSION != SCHM_SPI_H_SW_MINOR_VERSION) || \
//     (SPI_C_SW_PATCH_VERSION != SCHM_SPI_H_SW_PATCH_VERSION)    \
//    )
//    #error "Software Version Numbers of Spi.c and SchM_Spi.h are different"
//#endif

#if defined(SPI_DEV_ERROR_DETECT) && (SPI_DEV_ERROR_DETECT == STD_ON)
    /* Check if Spi.c and Det.h file are of the same Autosar version */
    #if ((SPI_C_AR_RELEASE_MAJOR_VERSION    != DET_H_AR_RELEASE_MAJOR_VERSION) || \
         (SPI_C_AR_RELEASE_MINOR_VERSION    != DET_H_AR_RELEASE_MINOR_VERSION) || \
         (SPI_C_AR_RELEASE_REVISION_VERSION != DET_H_AR_RELEASE_REVISION_VERSION) \
        )
        #error "AutoSar Version Numbers of Spi.c and Det.h are different"
    #endif    
    /* Check if Spi.c and Det.h header file are of the same Software version */
    #if ((SPI_C_SW_MAJOR_VERSION != DET_H_SW_MAJOR_VERSION) || \
         (SPI_C_SW_MINOR_VERSION != DET_H_SW_MINOR_VERSION) || \
         (SPI_C_SW_PATCH_VERSION != DET_H_SW_PATCH_VERSION)    \
        )
        #error "Software Version Numbers of Spi.c and Det.h are different"
    #endif
#endif /*defined(SPI_DEV_ERROR_DETECT) && (SPI_DEV_ERROR_DETECT == STD_ON)*/

/* Check if Spi.c and Spi_Hal.h file are of the same Autosar version */
#if ((SPI_C_AR_RELEASE_MAJOR_VERSION    != SPI_HAL_H_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_C_AR_RELEASE_MINOR_VERSION    != SPI_HAL_H_AR_RELEASE_MINOR_VERSION) || \
     (SPI_C_AR_RELEASE_REVISION_VERSION != SPI_HAL_H_AR_RELEASE_REVISION_VERSION) \
    )
    #error "AutoSar Version Numbers of Spi.c and Spi_Hal.h are different"
#endif
/* Check if Spi.c and Spi_Hal.h header file are of the same Software version */
#if ((SPI_C_SW_MAJOR_VERSION != SPI_HAL_H_SW_MAJOR_VERSION) || \
     (SPI_C_SW_MINOR_VERSION != SPI_HAL_H_SW_MINOR_VERSION) || \
     (SPI_C_SW_PATCH_VERSION != SPI_HAL_H_SW_PATCH_VERSION)    \
    )
    #error "Software Version Numbers of Spi.c and Spi_Hal.h are different"
#endif

/* Check if Spi.c and Spi_Cfg.h file are of the same Autosar version */
#if ((SPI_C_AR_RELEASE_MAJOR_VERSION    != SPI_CFG_H_AR_RELEASE_MAJOR_VERSION) || \
     (SPI_C_AR_RELEASE_MINOR_VERSION    != SPI_CFG_H_AR_RELEASE_MINOR_VERSION) || \
     (SPI_C_AR_RELEASE_REVISION_VERSION != SPI_CFG_H_AR_RELEASE_REVISION_VERSION) \
    )
    #error "AutoSar Version Numbers of Spi.c and Spi_Cfg.h are different"
#endif
/* Check if Spi.c and Spi_Cfg.h header file are of the same Software version */
#if ((SPI_C_SW_MAJOR_VERSION != SPI_CFG_H_SW_MAJOR_VERSION) || \
     (SPI_C_SW_MINOR_VERSION != SPI_CFG_H_SW_MINOR_VERSION) || \
     (SPI_C_SW_PATCH_VERSION != SPI_CFG_H_SW_PATCH_VERSION)    \
    )
    #error "Software Version Numbers of Spi.c and Spi_Cfg.h are different"
#endif




/*==================================================================================================
*                         LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/


/*==================================================================================================
*                                       LOCAL MACROS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      LOCAL VARIABLES
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL CONSTANTS
==================================================================================================*/


/*==================================================================================================
*                                      GLOBAL VARIABLES
==================================================================================================*/
#define SPI_START_SEC_VAR_INIT_UNSPECIFIED
#include "Spi_MemMap.h"

P2CONST(Spi_ConfigType, SPI_VAR, SPI_APPL_CONST) Spi_pConfig = NULL_PTR; 

#define SPI_STOP_SEC_VAR_INIT_UNSPECIFIED
#include "Spi_MemMap.h"

/*==================================================================================================
*                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/
FUNC (void, SPI_CODE) Spi_ScheduleJob(P2VAR(Spi_JobConfigType, SPI_VAR, SPI_APPL_CONST) pJob);

/*==================================================================================================
*                                      LOCAL FUNCTIONS
==================================================================================================*/
#define SPI_START_SEC_CODE
#include "Spi_MemMap.h"


/*==================================================================================================
*                                      GLOBAL FUNCTIONS
==================================================================================================*/
/**
* @brief   This function initializes the SPI driver.
* @details This function initializes the SPI driver using the
*          pre-established configurations
*          - Service ID:       0x00
*          - Sync or Async:       Synchronous
*          - Reentrancy:       Non-Reentrant
*
* @param[in]     ConfigPtr      Specifies the pointer to the configuration set
*
* @implements Spi_Init_Activity
*/
FUNC (void, SPI_CODE) Spi_Init
    (
        P2CONST(Spi_ConfigType, AUTOMATIC, SPI_APPL_CONST) ConfigPtr
    )
{
    uint8   u8Index = 0;

#if (SPI_DEV_ERROR_DETECT == STD_ON)
    if (Spi_pConfig != NULL_PTR)
    {
        (void)Det_ReportError(SPI_MODULE_ID, SPI_INSTANCE_ID, SPI_SERVICE_INIT_ID, SPI_E_ALREADY_INITIALIZED);
    }
#if (SPI_PRECOMPILE_SUPPORT == STD_OFF)
    else if (NULL_PTR == ConfigPtr)
#else /*(SPI_PRECOMPILE_SUPPORT == STD_OFF) */
    else if (NULL_PTR != ConfigPtr)
#endif /* (SPI_DEV_ERROR_DETECT == STD_ON) */
    {
        (void)Det_ReportError(SPI_MODULE_ID, SPI_INSTANCE_ID, SPI_SERVICE_INIT_ID, SPI_E_UNINIT);
    }
    else
#endif /* (STD_ON == SPI_DEV_ERROR_DETECT) */
    {

#if (SPI_PRECOMPILE_SUPPORT == STD_ON)
        ConfigPtr = &Spi_PBCfgVariantPredefined;
#endif

        for (u8Index = 0; u8Index < SPI_TOTAL_PHY_UNIT; u8Index++)
            Spi_HAL_HWInit(&Spi_PhyHwUnits[u8Index]);
        /*  Save configuration pointer in global variable */
        Spi_pConfig = ConfigPtr;
    }     
}


/**
* @brief   This function setup an external buffer to be used by a specific channel.
* @details This function setup an external buffer to be used by a specific channel.
*          - Service ID:       0x05
*          - Sync or Async:       Synchronous
*          - Reentrancy:       Reentrant
*
* @param[in]      Channel             Channel ID
* @param[in]      SrcDataBufferPtr    Pointer to the memory location that will hold
*                                     the transmitted data
* @param[in]      Length              Length of the data in the external buffer
* @param[out]     DesDataBufferPtr    Pointer to the memory location that will hold
*                                     the received data
*
* @return Std_ReturnType
* @retval E_OK      Setup command has been accepted
* @retval E_NOT_OK  Setup command has not been accepted
*
* @pre  The driver needs to be initialized before calling Spi_SetupEB()
*       otherwise, the function Spi_SetupEB() shall raise the development error
*       if SPI_DEV_ERROR_DETECT is STD_ON.
* @pre  Pre-compile parameter SPI_CHANNEL_BUFFERS_ALLOWED shall be USAGE1 or USAGE2.
*
* @implements Spi_SetupEB_Activity
*/
FUNC (Std_ReturnType, SPI_CODE) Spi_SetupEB(
        VAR(Spi_ChannelType, AUTOMATIC) Channel,
        P2CONST(Spi_DataBufferType, AUTOMATIC, SPI_APPL_DATA) SrcDataBufferPtr,
        P2VAR(Spi_DataBufferType, AUTOMATIC, SPI_APPL_DATA) DestDataBufferPtr,
        VAR(Spi_NumberOfDataType, AUTOMATIC) Length
    )
{
    VAR(Std_ReturnType, SPI_VAR) Status = E_OK;
    
#if (SPI_DEV_ERROR_DETECT == STD_ON)
    if (Spi_pConfig == NULL)
    {
        (void)Det_ReportError(SPI_MODULE_ID, SPI_INSTANCE_ID, SPI_SERVICE_SETUPIB_ID, SPI_E_UNINIT);
        Status = E_NOT_OK;
    }
    else if (Channel > Spi_pConfig->u8SpiMaxChannel
        || (Spi_ChannelConfigs[Channel]->u8Config & SPI_CHANNEL_CFG_EB_TYPE) \
        == SPI_CHANNEL_CFG_IB_TYPE)
    {
        (void)Det_ReportError(SPI_MODULE_ID, SPI_INSTANCE_ID, SPI_SERVICE_SETUPIB_ID, SPI_E_PARAM_CHANNEL);
        Status = E_NOT_OK;
    }
    else if (SrcDataBufferPtr == NULL_PTR)
    {
        (void)Det_ReportError(SPI_MODULE_ID, SPI_INSTANCE_ID, SPI_SERVICE_SETUPIB_ID, SPI_E_PARAM_POINTER);
        Status = E_NOT_OK;
    }
    else
#endif /* (STD_ON == SPI_DEV_ERROR_DETECT) */
    {
        Spi_ChannelConfigs[Channel].pcBufferDescriptor->pBufferTX = \
            (P2VAR(Spi_DataBufferType, AUTOMATIC, SPI_APPL_DATA)) SrcDataBufferPtr;
        Spi_ChannelConfigs[Channel].pcBufferDescriptor->pBufferRX = DestDataBufferPtr;
        Spi_ChannelConfigs[Channel].u16BuffLength = Length;
    }    
    return Status;
}    

/**
* @brief   This function is used for synchronous transmission of a given sequence.
* @details This function is used for synchronous transmission of a given sequence.
*          - Service ID:       0x0a
*          - Sync or Async:       Synchronous
*          - Reentrancy:       Reentrant
*
* @param[in]      Sequence            Sequence ID
*
* @return Std_ReturnType
* @retval E_OK      Transmission command has been completed successfully
* @retval E_NOT_OK  Transmission command has not been accepted
*
* @pre  The driver needs to be initialized before calling Spi_SyncTransmit().
*       otherwise, the function Spi_SyncTransmit() shall raise the development error
*       if SPI_DEV_ERROR_DETECT is STD_ON.
* @pre  Pre-compile parameter SPI_LEVEL_DELIVERED shall be LEVEL0 or LEVEL2
*
* @implements Spi_SyncTransmit_Activity
*/
FUNC (Std_ReturnType, SPI_CODE) Spi_SyncTransmit(VAR(Spi_SequenceType, AUTOMATIC) Sequence)
{
    VAR(uint8, SPI_VAR) u8Index = 0;
    VAR(Std_ReturnType, SPI_VAR) Status = E_OK;
    VAR(uint8, SPI_VAr) i = 0;
    
#if (SPI_DEV_ERROR_DETECT == STD_ON)
    if (Spi_pConfig == NULL)
    {
        (void)Det_ReportError(SPI_MODULE_ID, SPI_INSTANCE_ID, SPI_SERVICE_SYNCTRANSMIT_ID, SPI_E_UNINIT);
        Status = E_NOT_OK;
    }
    else
#endif /* (STD_ON == SPI_DEV_ERROR_DETECT) */
    {
        for (u8Index = 0; u8Index < Spi_SequenceConfigs[Sequence].NumJobs; u8Index++)
        {
            for (i = 0; i < Spi_SequenceConfigs[Sequence].pcJobList[u8Index]->NumChannels; i++)
            {
                Spi_HAL_SyncTransmit(Spi_SequenceConfigs[Sequence].pcJobList[u8Index]->pDevice,
                    Spi_SequenceConfigs[Sequence].pcJobList[u8Index]->pcChannelList[i]);

            }
            if (Spi_SequenceConfigs[Sequence].pcJobList[u8Index]->EndJobNotification != NULL_PTR)
            {
            	Spi_SequenceConfigs[Sequence].pcJobList[u8Index]->EndJobNotification();
            }
        }
        
        if (Spi_SequenceConfigs[Sequence].EndSeqNotification != NULL_PTR)
        {
            Spi_SequenceConfigs[Sequence].EndSeqNotification();
        }


    }  
    return Status;
}

/**
* @brief   This function returns the version information for the SPI driver.
* @details This function returns the version information for the SPI driver.
*          - Service ID:       0x09
*          - Sync or Async:       Synchronous
*          - Reentrancy:       Non-Reentrant
*
* @param[in,out]    VersionInfo      Pointer to where to store the version
*                                    information of this module.
*
* @pre  Pre-compile parameter SPI_VERSION_INFO_API shall be STD_ON.
*
* @implements Spi_GetVersionInfo_Activity
*/
FUNC (void, SPI_CODE) Spi_GetVersionInfo(
    P2VAR(Std_VersionInfoType, AUTOMATIC, SPI_APPL_DATA) pVersionInfo
)
{
#if (MCU_DEV_ERROR_DETECT == STD_ON)
    if(NULL_PTR == pVersionInfo )
    {
        (void) Det_ReportError((uint16)SPI_MODULE_ID, 
                        SPI_INSTANCE_ID, 
                        SPI_SERVICE_GETVERSIONINFO_ID, 
                        SPI_E_PARAM_POINTER);
    }
    else
#endif /* (MCU_DEV_ERROR_DETECT == STD_ON) */
    {
        (pVersionInfo)->vendorID         = (uint16)SPI_C_VENDOR_ID;
        (pVersionInfo)->moduleID         = (uint16)SPI_MODULE_ID;
        (pVersionInfo)->sw_major_version = (uint8)SPI_C_SW_MAJOR_VERSION;
        (pVersionInfo)->sw_minor_version = (uint8)SPI_C_SW_MINOR_VERSION;
        (pVersionInfo)->sw_patch_version = (uint8)SPI_C_SW_PATCH_VERSION;

    } 
}





#define SPI_STOP_SEC_CODE
#include "Spi_MemMap.h"

#ifdef __cplusplus
}
#endif


/** @} */

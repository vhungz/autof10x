#define ASM_EXTERN(os_func) .extern os_func
#define ASM_GLOBAL(os_func)	.global os_func
#define ASM_TYPE(os_func,type_func)	.type	Irq_Handler, type_func
#define ASM_LABEL(os_func) os_func:
#define ASM_WORD(dd_data) .word dd_data
#define ASM_WEAK(f_func) .weak f_func
